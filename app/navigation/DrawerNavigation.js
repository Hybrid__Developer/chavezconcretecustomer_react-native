import { Dimensions } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
// import DrawerView from "./DrawerView";
import HomeScreen from '../screens/customerScreens/Home'
import SideBar from '../screens/customerScreens/SideBar';
import ChangePasswordScreen from "../screens/customerScreens/ChangePassword/index";
import MyProfileScreen from "../screens/customerScreens/MyProfile/index";
import MyBookingsScreen from "../screens/customerScreens/MyBookings/index";
import ScheduleUnitScreen from "../screens/customerScreens/ScheduleUnit/index";

// const screenWidth = Dimensions.get('window').width;
const MyDrawerNavigator = createDrawerNavigator({

    SideBar: {
        screen: SideBar, 
        navigationOptscheduleJobsScreenions: {
            header: null
        }
    },

    Home: {
      screen: HomeScreen,
      navigationOptions: {
          header: null
      }
  },
    ChangePassword:{
      screen:ChangePasswordScreen,
      navigationOptions:{
        header:null
      }
    },
    MyProfile:{
      screen:MyProfileScreen,
      navigationOptioscheduleJobsScreenns:{
        header:null
      }
    },

   
    ScheduleUnit:{
      screen: ScheduleUnitScreen,
      navigationOptions:{
        header:null
      }
    },


    MyBookings:{
      screen:MyBookingsScreen,
      navigationOptions:{
        header:null
      }
    },






  },





    {
        initialRouteName: 'Home',

        contentOptions: {
            style: {
                backgroundColor: 'white',
                flex: 1,
            }
        },
        navigationOptions: {
            drawerLockMode: 'locked-closed'
        },
        contentComponent: SideBar,
    },
);

export default MyApp = createAppContainer(MyDrawerNavigator);