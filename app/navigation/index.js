// import { createAppContainer, createSwitchNavigator } from 'react-navigation';
// import { createStackNavigator } from 'react-navigation-stack';
// import { View, Text, SafeAreaView } from "react-native";
// import React, { Component } from "react";
// import { createAppContainer, createSwitchNavigator } from 'react-navigation';
// import { createStackNavigator } from 'react-navigation-stack';
import { View, Text, SafeAreaView } from "react-native";
import React, { Component } from "react";
// import UpcomingScheduleDetailsScreen from "../screens/Operator/UpcomingScheduleDetails/index"
// import ManagerDrawerNavigation from "../navigation/ManagerDrawerNavigation";
// import SideBar from "../screens/Manager/SideBar"
// import SplashScreen from "../screens/Splash/index";
// import SignInScreen from "../screens/SignIn/index";
// import SignUpScreen from "../screens/SignUp/index";
// import HomeScreen from "../screens/Home/index";

//Operator imports
import OperatorHomeScreen from "../screens/Operator/OperatorHome/index";
import CompletedScheduleScreen from "../screens/Operator/CompletedSchedule/index";
import CompletedScheduleDetailsScreen from "../screens/Operator/CompletedScheduleDetails/index";
import OperatorMyProfileScreen from "../screens/Operator/MyProfile/index";
import OperatorChangePasswordScreen from "../screens/Operator/ChangePassword/index";
import UpcomingScheduleDetailsScreen from "../screens/Operator/UpcomingScheduleDetails/index";
import OperatorDrawerNavigation from "../navigation/OperatorDrawerNavigation";
//Manager imports
// import HomeScreen from "../screens/Manager/Home/index";
// import ScheduledJobsScreen from "../screens/Manager/ScheduledJobs/index";
// import PendingJobsScreen from "../screens/Manager/PendingJobs/index";
// import ScheduleDetailsScreen from "../screens/Manager/ScheduleDetails/index";
// import AssignOperatorScreen from "../screens/Manager/AssignOperator/index";
// import AddNewJobsScreen from "../screens/Manager/AddNewJob/index";
// import CustomerListScreen from "../screens/Manager/CustomerList/index";
// import ChangePasswordScreen from "../screens/Manager/ChangePassword/index";
// import MyProfileScreen from "../screens/Manager/MyProfile/index"
// @refresh reset
// const AppStack = createStackNavigator({ 

//   Home:{
//     screen:HomeScreen,
//     navigationOptions:{
//       header:null
//     }
//   },
//   Splash:{
//    screen:SplashScreen,
//    navigationOptions:{
//      header: null

//     }
//   },
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import SplashScreen from "../screens/customerScreens/Splash/index";
import SignInScreen from "../screens/customerScreens/SignIn/index";
import SignUpScreen from "../screens/customerScreens/SignUp/index";
import HomeScreen from "../screens/customerScreens/Home/index";
import ChangePasswordScreen from "../screens/customerScreens/ChangePassword/index";
import MyProfileScreen from "../screens/customerScreens/MyProfile/index";
import ScheduleUnitScreen from "../screens/customerScreens/ScheduleUnit/index";
import concreteCalciScreen from "../screens/customerScreens/ConcreteCacli/index";
import scheduleDetailsScreen from "../screens/customerScreens/ScheduleDetails/index";
import MyBookingsScreen from "../screens/customerScreens/MyBookings/index";
import DrawerNavigation from "../navigation/DrawerNavigation";
import SideBar from "../screens/customerScreens/SideBar"




// @refresh reset
// const AppStack = createStackNavigator({

//   Splash: {
//     screen: SplashScreen,
//     navigationOptions: {
//       header: null

//     }
//   },

//   SignIn: {
//     screen: SignInScreen,
//     navigationOptions: {
//       header: null

//     }
//   },
//   SignUp: {
//     screen: SignUpScreen,
//     navigationOptions: {
//       header: null
//     }
//   },
//   Home: {
//     screen: HomeScreen,
//     navigationOptions: {
//       header: null
//     }
//   },
//   concreteCalci: {
//     screen: concreteCalciScreen,
//     navigationOptions: {
//       header: null
//     }
//   },
//   ChangePassword: {
//     screen: ChangePasswordScreen,
//     navigationOptions: {
//       header: null
//     }
//   },
//   MyProfile: {
//     screen: MyProfileScreen,
//     navigationOptions: {
//       header: null
//     }
//   },
//   ScheduleUnit: {
//     screen: ScheduleUnitScreen,
//     navigationOptions: {
//       header: null
//     }
//   },

//   scheduleDetails: {
//     screen: scheduleDetailsScreen,
//     navigationOptions: {
//       header: null
//     }
//   },
//   MyBookings: {
//     screen: MyBookingsScreen,
//     navigationOptions: {
//       header: null
//     }
//   },
//   SideBar: {
//     screen: SideBar,
//     navigationOptions: {
//       header: null
//     }
//   },




// },

//   {
//     initialRouteName: 'Splash',
//   });



// const AuthStack = createStackNavigator({ 


//   SignIn: {

//     screen:SignInScreen,
//     navigationOptions: {
//         header: null

//     }
// },




//OPERATOR STACK

// const OperatorAppStack = createStackNavigator({
//   Home:{
//         screen:HomeScreen,
//         navigationOptions:{
//          header: null
//         }
//       },
//       CompletedSchedule:{
//         screen:CompletedScheduleScreen,
//         navigationOptions:{
//          header: null
//         }
//       },
//       UpcomingScheduleDetails:{
//         screen:UpcomingScheduleDetailsScreen,
//         navigationOptions:{
//          header: null
//         }
//       },
//       CompletedScheduleDetails :{
//         screen:CompletedScheduleDetailsScreen,
//         navigationOptions:{
//          header: null
//         }
//       },

// },



//   {
//     initialRouteName: 'Home',

// })


// MANAGER STACK


// const ManagerAppStack = createStackNavigator({
//   Home: {
//     screen: HomeScreen,
//     navigationOptions: {
//       header: null
//     }
//   },
//   ScheduledJobs: {
//     screen: ScheduledJobsScreen,
//     navigationOptions: {
//       header: null
//     }
//   },
//   PendingJobs: {
//     screen: PendingJobsScreen,
//     navigationOptions: {
//       header: null
//     }
//   },
//   ScheduleDetails: {
//     screen: ScheduleDetailsScreen,
//     navigationOptions: {
//       header: null
//     }
//   },
//   AssignOperator: {
//     screen: AssignOperatorScreen,
//     navigationOptions: {
//       header: null
//     }
//   },
//   AddNewJob: {
//     screen: AddNewJobsScreen,
//     navigationOptions: {
//       header: null
//     },
//   },
//   CustomerList: {
//     screen: CustomerListScreen,
//     navigationOptions: {
//       header: null
//     },
//   },
//   ChangePassword: {
//     screen: ChangePasswordScreen,
//     navigationOptions: {
//       header: null
//     },
//   },
//   MyProfile: {
//     screen: MyProfileScreen,
//     navigationOptions: {
//       header: null
//     },
//   },
//   SideBar: {
//     screen: SideBar,
//     navigationOptions: {
//       header: null
//     }
//   },
// },

//   {
//     initialRouteName: 'Home',
const OperatorAppStack = createStackNavigator({
  OperatorHome: {
    screen: OperatorHomeScreen,
    navigationOptions: {
      header: null
    }
  },
  CompletedSchedule: {
    screen: CompletedScheduleScreen,
    navigationOptions: {
      header: null
    }
  },
  UpcomingScheduleDetails: {
    screen: UpcomingScheduleDetailsScreen,
    navigationOptions: {
      header: null
    }
  },
  CompletedScheduleDetails: {
    screen: CompletedScheduleDetailsScreen,
    navigationOptions: {
      header: null
    }
  },
  OperatorChangePassword: {
    screen: OperatorChangePasswordScreen,
    navigationOptions: {
      header: null
    }
  },
  OperatorMyProfile: {
    screen: OperatorMyProfileScreen,
    navigationOptions: {
      header: null
    }
  },
},



  {
    initialRouteName: 'OperatorHome',
  })
//   })

// });


// MANAGER STACK


// const ManagerAppStack = createStackNavigator({
//   Home: {
//     screen: HomeScreen,
//     navigationOptions: {
//       header: null
//     }
//   },
//   ScheduledJobs: {
//     screen: ScheduledJobsScreen,
//     navigationOptions: {
//       header: null
//     }
//   },
//   PendingJobs: {
//     screen: PendingJobsScreen,
//     navigationOptions: {
//       header: null
//     }
//   },
//   ScheduleDetails: {
//     screen: ScheduleDetailsScreen,
//     navigationOptions: {
//       header: null
//     }
//   },
//   AssignOperator: {
//     screen: AssignOperatorScreen,
//     navigationOptions: {
//       header: null
//     }
//   },
//   AddNewJob: {
//     screen: AddNewJobsScreen,
//     navigationOptions: {
//       header: null
//     },
//   },
//   CustomerList: {
//     screen: CustomerListScreen,
//     navigationOptions: {
//       header: null
//     },
//   },
//   ChangePassword: {
//     screen: ChangePasswordScreen,
//     navigationOptions: {
//       header: null
//     },
//   },
//   MyProfile: {
//     screen: MyProfileScreen,
//     navigationOptions: {
//       header: null
//     },
//   },
//   SideBar: {
//     screen: SideBar,
//     navigationOptions: {
//       header: null
//     }
//   },
// },

//   {
//     initialRouteName: 'Home',

//   })

const Routes = createAppContainer(
  createSwitchNavigator(
    {
      // AuthLoading: AuthLoadingScreen,
      // Auth: AuthStack,
      // App: ManagerAppStack,
      // ManagerDrawerNavigation: ManagerDrawerNavigation,
      // App: AppStack,
      // DrawerNavigation: DrawerNavigation,
      OperatorApp: OperatorAppStack,
      OperatorDrawerNavigation: OperatorDrawerNavigation
      // Auth: AuthStack,
    },


    // Auth: AuthStack,

  )
);
export default Routes