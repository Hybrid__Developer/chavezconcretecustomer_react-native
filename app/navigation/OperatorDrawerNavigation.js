//Operator IMports
import { Dimensions } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import OperatorHomeScreen from '../screens/Operator/OperatorHome/'
import OperatorSideBar from '../screens/Operator/SideBar'
import ChangePasswordScreen from "../screens/Operator/ChangePassword/index";
import MyProfileScreen from "../screens/Operator/MyProfile/index"
import CompletedScheduleScreen from "../screens/Operator/CompletedSchedule/index"


const MyDrawerNavigator = createDrawerNavigator({

    OperatorSideBar: {
        screen: OperatorSideBar,
        navigationOptions: {
            header: null
        }
    },

    OperatorHome: {
        screen: OperatorHomeScreen,
        navigationOptions: {
            header: null
        }
    },
    ChangePassword: {
        screen: ChangePasswordScreen,
        navigationOptions: {
            header: null
        }
    },
    MyProfile: {
        screen: MyProfileScreen,
        navigationOptions: {
            header: null
        }
    },
    CompletedSchedule: {
        screen: CompletedScheduleScreen,
        navigationOptions: {
            header: null
        }
    },

},
    {
        initialRouteName: 'OperatorHome',

        contentOptions: {
            style: {
                backgroundColor: 'white',
                flex: 1,
            }
        },
        navigationOptions: {
            drawerLockMode: 'locked-closed'
        },
        contentComponent: OperatorSideBar,
    },
);

export default MyApp = createAppContainer(MyDrawerNavigator);