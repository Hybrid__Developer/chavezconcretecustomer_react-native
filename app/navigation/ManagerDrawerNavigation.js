import { Dimensions } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
// import DrawerView from "./DrawerView";
// import HomeScreen from '../screens/customerScreens/Home'


//Operator IMports


//Manager Imports
import HomeScreen from '../screens/Manager/Home'
import SideBar from '../screens/Manager/SideBar'
import CustomerListScreen from "../screens/Manager/CustomerList/index";
import ChangePasswordScreen from "../screens/Manager/ChangePassword/index";
import MyProfileScreen from "../screens/Manager/MyProfile/index"
import AddNewJobsScreen from "../screens/Manager/AddNewJob/index";


//Customer Imports
// import ChangePasswordScreen from "../screens/customerScreens/ChangePassword/index";
// import MyProfileScreen from "../screens/customerScreens/MyProfile/index";
// import scheduleJobsScreen from "../screens/customerScreens/ScheduleJobs/index";
// import ScheduleUnitScreen from "../screens/customerScreens/ScheduleUnit/index";

// const screenWidth = Dimensions.get('window').width;
const MyDrawerNavigator = createDrawerNavigator({

    SideBar: {
        screen: SideBar,
        navigationOptions: {
            header: null
        }
    },

    Home: {
        screen: HomeScreen,
        navigationOptions: {
            header: null
        }
    },
    ChangePassword: {
        screen: ChangePasswordScreen,
        navigationOptions: {
            header: null
        }
    },
    MyProfile: {
        screen: MyProfileScreen,
        navigationOptions: {
            header: null
        }
    },
    CustomerList: {
        screen: CustomerListScreen,
        navigationOptions: {
            header: null
        },
    },

    AddNewJob: {
        screen: AddNewJobsScreen,
        navigationOptions: {
            header: null
        },
    },
    MyProfile: {
        screen: MyProfileScreen,
        navigationOptions: {
            header: null
        },
    },


},
    {
        initialRouteName: 'Home',

        contentOptions: {
            style: {
                backgroundColor: 'white',
                flex: 1,
            }
        },
        navigationOptions: {
            drawerLockMode: 'locked-closed'
        },
        contentComponent: SideBar,
    },
);

export default MyApp = createAppContainer(MyDrawerNavigator);