import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button,Title, Segment, Content, Text, Card, CardItem, View } from 'native-base';
// import styles from './style';
import styles from './style';
import * as colors from "../../../constants/colors"
import Icon from 'react-native-vector-icons/FontAwesome';



import { ScrollView } from 'react-native-gesture-handler';
import{TextInput,TouchableOpacity} from 'react-native'
export default class ScheduleDetails extends Component {
    render(){
        return(
           
            <Container>
                <Header style={{ backgroundColor: '#fff' }}>
                    <Left>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                    <Icon size={28} style={{ color: '#000', }} name='angle-left' />
                    </TouchableOpacity>
                    </Left>
                    <Title style={styles.HeaderTitle}>Schedule Details</Title>
                    <Right>
                        <Button transparent>
                        <Icon size={24} style={{ color: '#b61925' }} name='bell' />
                        </Button>
                    </Right>
                </Header>
                <ScrollView>
               <View style={{flexDirection:'row',marginTop:'5%'}}> 
                   <View style={{marginLeft:'5%'}} >
                       <Text style={{fontSize:20, fontWeight:'bold',color:colors.primaryColor}}>Job Status: </Text>
                   </View>
                   <View style={{marginTop:'1%'}}>
                       <Text style={{fontWeight:'bold',fontSize:16}}>Pending </Text>
                   </View>
               </View>

                <View style={styles.CustomerDetailView}>
                <View style={{ flexDirection: "row" }}> 
                <View style={ styles.CustomerDetailLeftView}>
               
                                        <Text style={styles.CustomerDetailLeftText}>
                                            Customer Name:
                                </Text>
                                <Text style={styles.CustomerDetailLeftText}>
                                            Contact Number:
                                </Text>
                                        <Text style={styles.CustomerDetailLeftText}>
                                        Arrival Date:
                                </Text>
                                        <Text style={styles.CustomerDetailLeftText}>
                                        Start Time:
                                </Text>
                                <Text style={styles.CustomerDetailLeftText}>
                                            {/* {item.jobAddress} */}
                                            Job Address:
                                        </Text>
                                    </View>
                                    <View style={styles.CustomerDetailRightView}>
                                        
                                        <Text style={styles.CustomerDetailRightText}>
                                            {/* {item.name} */}zxc
                                        </Text>
                                        <Text style={styles.CustomerDetailRightText}>
                                            {/* {item.jobAddress} */}123456
                                        </Text>
                                        <Text style={styles.CustomerDetailRightText}>
                                            {/* {item.phoneNumber} */}5/12/2019
                                        </Text>
                                        <Text style={styles.CustomerDetailRightText}>
                                            {/* {item.phoneNumber} */}10:00 A.M
                                        </Text>
                                        <Text style={styles.CustomerDetailRightText}>
                                            {/* {item.phoneNumber} */}xyz
                                        </Text>
                </View>

                </View>
                </View>

                <View style={styles.EquipmentDetailslView}>
                     <Text style={styles.EquipmentDetailsHeading}>Equipment Details:</Text>
                     <View style={{marginTop:'5%',flexDirection:'row', height:24}}>
                       <View style={{marginLeft: '19%',justifyContent:'flex-start',}}>
                           <Text style={{fontSize:18}}>3" X 10' system:12</Text>
                       </View>
                  
                   </View>
                </View>
                <View style={styles.JobConditionView}>
                        <Text style={styles.JobConditionHeading}>Job Condition & Notes: </Text>
                        <Text style={styles.JobConditionText}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the </Text>
                </View>
                </ScrollView>
            </Container>
        );
    }
}