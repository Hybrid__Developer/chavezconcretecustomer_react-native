import { Dimensions, StyleSheet, } from 'react-native';
import * as colors from "../../../constants/colors"
const window = Dimensions.get('window');


const styles = StyleSheet.create({
       
    HeaderTitle:{
        color:colors.primaryColor ,
        fontSize: 18,
        fontWeight:'bold', 
        justifyContent:'center', 
        alignSelf:'center',
        marginLeft: '18%'
    },

    Button1:{
        borderColor:colors.primaryColor,
        height:30, 
        marginRight:5,
        marginLeft:5,
        
    },
    Button2:{
        borderColor:colors.primaryColor,
        height:30,
        marginRight:5
    },
    ButtonText:{
        color:colors.primaryColor
    },
    CustomerDetailView:{
        height: 150,
        width: '96%',
        // backgroundColor:'red',
        borderWidth: 1,
        marginLeft: '2%',
        borderColor:colors.primaryColor,
        borderRadius: 10,
        shadowColor: 'black',
        // shadowOffset: { width: 5, height: 5 },
        // shadowOpacity: 0.5,
        // shadowRadius: 2,
        elevation: 3,
        marginBottom: 10,
        marginTop: '5%'
    },
    scheduleTextInput:{borderWidth:1,borderColor:'#05cccf', height:100,width:140,marginLeft:'13%', marginTop:'4%'},
    CustomerDetailLeftView :{
        flex: 1, 
        justifyContent: "flex-start", 
        alignItems: "flex-start", 
        marginRight: 5,
        marginTop:10,
        marginLeft:'5%',
    },
    CustomerDetailRightView:{
        flex: 1, 
        justifyContent: "flex-start", 
        alignItems: "flex-start", 
        marginLeft: 5,
        marginTop:10
    },
    CustomerDetailLeftText:{
        justifyContent: 'flex-end'
    },
    CustomerDetailRightText:{
        justifyContent: 'flex-start'
    },
    EquipmentDetailslView:{
        height: 200,
        width: '96%',
        // backgroundColor:'red',
        borderWidth: 1,
        marginLeft: '2%',
        borderColor:colors.primaryColor,
        borderRadius: 10,
        shadowColor: 'black',
        shadowOffset: { width: 5, height: 5 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 2,
        marginBottom: 10,
        marginTop: '5%'
    },
    EquipmentDetailsHeading:{
        justifyContent:'flex-start',
        marginLeft:"5%",
         fontWeight:'bold'
    },
    JobConditionView:{
        height: 100,
        width: '96%',
        // backgroundColor:'red',
        borderWidth: 1,
        marginLeft: '2%',
        borderColor:colors.primaryColor,
        borderRadius: 10,
        shadowColor: 'black',
        shadowOffset: { width: 5, height: 5 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 2,
        marginBottom: 10,
        marginTop: '5%'
    },
    JobConditionHeading:{
        marginLeft:'5%',
         fontWeight:'bold'
    },
    JobConditionText:{
        padding:'2%',
        letterSpacing:0.8
    },
})
export default styles