import React, { Component } from "react";
import { View,Image,ImageBackground,SafeAreaView } from "react-native";
import styles from "./style";
import * as utility from "../../../utility/index"
export default class Splash extends Component {
  componentDidMount() {
    this.timeoutHandle = setTimeout(() => {
        this.retrieveData()
    }, 2000);

}
retrieveData() {
    this.props.navigation.navigate('SignIn')
}
constructor(props) {
    super(props);
    this.state = {
        isLoading: true
     }
 }
componentWillUnmount() {
    clearTimeout(this.timeoutHandle);
}
  render() {
    return (
 
      <View style={styles.wrapper}>
         <ImageBackground source={require('../../../assets/bg.png')} style={styles.container} resizeMode="cover">
         <View style={styles.logoContainer}>
                        <Image source={require('../../../assets/logo.png')} style={styles.logo} />
                    </View>
                    </ImageBackground>
      </View>
    
    );
  }
}
