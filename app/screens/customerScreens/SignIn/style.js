import { Dimensions, StyleSheet, } from 'react-native';
const window = Dimensions.get('window');
import * as colors from "../../../constants/colors"
import * as utility from "../../../utility/index"
import constants from 'jest-haste-map/build/constants';

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
      },
      container: {
        // flex:1,
        // alignItems: 'center',
        // //justifyContent: 'center',
        // alignSelf:'stretch',
        height: utility.deviceHeight,
        width: utility.deviceWidth
      },
      backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
      },
      logoContainer: {
        alignItems: 'center',
        alignSelf: 'stretch',
        //marginTop:30,
        justifyContent: 'center',
        // flexGrow: 1,
        marginTop: 35,
      },
      logo: {
        width: 250,
        height: 190,
      },
    SectionStyle: {
        backgroundColor: '#f2dede',
        color: 'white',
     
    
        // borderRadius: 10,
        elevation: 8,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
    },
    SectionStyle2: {
        backgroundColor: '#f2dede',
        // borderRadius: 8,
        marginLeft:'5%',
        marginRight:'5%',
        elevation: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
    },
    ImageStyle: {
        padding: 10,
        margin: 5,
        color: '#000'
    },
    modalView: {
        backgroundColor: '#f2dede',
        height: utility.deviceHeight/2,
        width:'85%',
        borderRadius: 8,
        borderColor: '#dfdfdf',
       
        marginTop:'20%',
        marginLeft:'8%',
        marginRight:'8%'
        // marginRight: 10,
    },
    loginTxtInput: {
        backgroundColor: 'transparent',
        color: '#000',
        flex: 1
    },
    InputBar: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    LoginBtn: {
        height: 50,
        backgroundColor:'#fff',
        marginTop: 10,
        borderRadius:30,
        // borderRadius: 8,
        borderColor: colors.primaryColor ,
        // borderWidth: 1,
        elevation: 10,
        marginLeft: 50,
        marginRight: 50,

    },
    forgotBtn: {
        height: 50,
        backgroundColor:'#f2dede' ,
        marginTop: 20,
        borderRadius:40,
        elevation: 10,
        marginLeft: 40,
        marginRight: 40,

    },
    AJ: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    LoginBtnTxt: {
        color:colors.primaryColor,
        fontSize: 25,
        textAlign: 'center',
        fontWeight:'bold'
    },
    LoginBtn1: {
        backgroundColor: '#ca0606',
        marginLeft: 70,
        marginRight: 60,
        borderColor: 'white',
        borderRadius: 10,
        borderWidth: 1,
    },
    SectionStyle1: {
        color: 'white',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',

    },
    LoginBtnTxt1: {
        color: '#fff',
        fontSize: 18,
        textAlign: 'center',

    },
    ForgotText :{
        color:colors.primaryColor ,
     fontWeight:'600',
     marginLeft:'20%',
     fontSize:23,
     marginTop:'1%'
    },
    Line:{
        borderBottomColor: colors.primaryColor,
        borderBottomWidth: 1,
        marginTop:'2%'
    },
})
export default styles
