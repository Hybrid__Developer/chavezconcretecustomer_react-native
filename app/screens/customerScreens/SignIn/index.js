import React, { Component } from "react";
import { View, Modal,Dimensions, Text, SafeAreaView, TextInput, ScrollView, ImageBackground,
     Image, TouchableOpacity, alert } from "react-native";

import styles from "./style";
import Icon from 'react-native-vector-icons/FontAwesome'
import * as commonApi from "../../../store/commonApi"
import * as api from "../../../constants/api"
import * as utility from "../../../utility/index"
// import * as deviceWidth from "../../../utility/index"


export default class SignIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            modalVisible: false,
        }

    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }
    login = () => {
        console.log("hello")
        if (utility.isFieldEmpty("")) {
            console.log("is field empty")
            if (utility.isValidEmail("")) {
                alert("Please enter valid email address")
            }

            const options = {
                headers: {
                    "Content-Type": "application/json",
                }
            }
            body = {
                email: this.state.email,
                passwd: this.state.password,

            }
            console.log("body", body)
            let response = commonApi.postDataApi(api.BASE_URL + "/users/login", body, options)
            console.log("login -response", response)
            // this.props.navigation.navigate('Home')

        }
        else {
            alert("please enter all the fields")
        }

    }
    otp = () => {
        console.log("otp")
        if (utility.isFieldEmpty("")) {
            console.log("is field empty")
            if (utility.isValidEmail("")) {
                Alert.alert("Please enter valid email address")
            }
            console.log("body", body)
            let response = commonApi.postDataApi(api.BASE_URL + "/users/otp", body, options)
            console.log("OTP -response", response)

        }
        else {
            console.log("herreee")
            alert("Please enter the email!")
        }

    }


    render() {

        return (
            <SafeAreaView behavior="padding" style={styles.wrapper} >
                <ImageBackground source={require('../../../assets/bg.png')}  style={styles.backgroundImage}>
                    <View style={styles.logoContainer}>
                        <Image source={require('../../../assets/logo.png')} style={styles.logo} />
                    </View>

                        <View style={{ marginTop: "7%" }}>

                            <View style={{ paddingLeft: '8%', paddingRight: '8%', marginTop: '10%' }}>
                                <View style={{ paddingBottom: '8%' }}>
                                    <View style={styles.SectionStyle}>
                                        <Icon name='envelope' size={20} style={styles.ImageStyle} />
                                        <TextInput require
                                            placeholder=' Email Address'
                                            placeholderTextColor='#000'
                                            onChangeText={(email) => this.setState({ email })}
                                            style={styles.loginTxtInput}
                                        />
                                    </View>
                                </View>

                                <View>
                                    <View style={styles.SectionStyle}>
                                        <Icon name='lock' size={30} style={styles.ImageStyle} />
                                        <TextInput require
                                            placeholder=' Password'
                                            placeholderTextColor='#000'
                                            onChangeText={(passwd) => this.setState({ passwd })}
                                            secureTextEntry
                                            style={styles.loginTxtInput}
                                        />
                                    </View>
                                </View>
                            </View>
                            <View style={{ paddingLeft: "54%", marginTop: '3%' }}>
                                <TouchableOpacity onPress={() => this.setModalVisible(true)}>

                                    <Text style={{
                                        color: 'grey',
                                        fontSize: 17,
                                        fontWeight:'700'
                                    }}>Forgot password?
                                </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{marginTop:'6%'}} >
                                <TouchableOpacity style={[styles.LoginBtn, styles.AJ]} onPress={this.login()}>
                                    <Text style={styles.LoginBtnTxt}>LOGIN</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ paddingLeft: "18%", flexDirection: 'row', marginTop:'20%'}}>
                                <Text style={{
                                    color: '#000',
                                    fontSize: 17,
                                    // fontWeight: 'bold'
                                }}>Don't Have an account?
                                    </Text>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('SignUp')} >
                                    <Text style={{  color: '#000',  fontSize: 17,   textDecorationLine: 'underline'  }}> SignUp </Text>
                                    </TouchableOpacity>
                                    </View> 
                                    </View>
                                    
                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={this.state.modalVisible}
                            onRequestClose={() => {Alert.alert('Modal has been closed.')}}>
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <View style={styles.modalView}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={styles.ForgotText}>Forgot Password</Text>
                                        <TouchableOpacity style={{ marginLeft: '16%', marginTop: '2%'}}
                                            onPress={() => {
                                                this.setModalVisible(!this.state.modalVisible);
                                            }}>
                                            <Text style={{ fontSize: 20, fontWeight: "bold"}}>X</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.Line}></View>
                                        <Text style={{ marginLeft:20,marginRight:20, marginTop:20, fontSize:14}}> Enter your email,and you will recieve an 
                                        OTP to create a new password on your email.
                                         </Text>
                                        {/* <Text style={{marginLeft:50, fontSize:15}}>OTP 
                                        to reset your password. </Text> */}
                                    <View style={{ paddingBottom: '3%', marginTop:30 }}>
                                        <View style={styles.SectionStyle2}>
                                            <Icon name='envelope' size={20} style={styles.ImageStyle} />
                                            <TextInput require
                                                placeholder=' Email'
                                                placeholderTextColor='#000'
                                                onChangeText={(email) => this.setState({ email })}
                                                style={styles.loginTxtInput}
                                            />
                                        </View>
                                    </View>
                                    <TouchableOpacity style={[styles.forgotBtn, styles.AJ]}
                                        onPress={() => {this.setModalVisible(!this.state.modalVisible)}}>
                                        <Text style={styles.LoginBtnTxt}>SUBMIT</Text>
                                    </TouchableOpacity></View>
                                <View>
                                </View>
                            </View>

                        </Modal>

                   
                </ImageBackground>
            </SafeAreaView>

        );
    }
}

