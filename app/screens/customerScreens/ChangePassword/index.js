import React, { Component } from 'react';
import { Container, Header, Left,Drawer, Body, Right, Button, Title, Content, Input, Item, Text } from 'native-base';
import styles from './style'
import { View, TouchableOpacity } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
import { SafeAreaView } from 'react-native'
import * as utility from "../../../utility/index"
import SideBar from "../SideBar"
// import { createDrawerNavigator } from 'react-navigation'  
export default class changePassword extends Component {
 
  render() {
    return (
      <SafeAreaView style={{ height: utility.deviceHeight, width: utility.deviceWidth }} >
        <Container>
         
            <Header style={{ backgroundColor: '#fff' }}>

              <Left>

              <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                    <Icon size={28} style={{ color: '#000', }} name='angle-left' />
                    </TouchableOpacity>
              </Left>
              <Body>
                <Title style={styles.HeaderTitle}>CHANGE  PASSWORD</Title>
              </Body>
              <Right>
                <Button transparent>
                <Icon size={24} style={{ color: '#000' }} name='bell' />
                </Button>
              </Right>
            </Header>

            <Content style={{ marginTop: '15%' }}>
              <Item regular style={style = styles.itemView}>
                <Input
                  secureTextEntry
                  placeholder='Old Password' />
              </Item>
              <Item regular style={styles.itemView}>
                <Input
                  secureTextEntry
                  placeholder='New Password' />
              </Item>
              <Item regular style={styles.itemView}>
                <Input
                  secureTextEntry
                  placeholder='Confirm Password' />
              </Item>
              <View style={{ marginTop: '20%', height: 100 }}>
                <TouchableOpacity style={[styles.LoginBtn, styles.AJ]} onPress={() => this.props.navigation.navigate('ChangePassword')}>
                  <Text style={styles.LoginBtnTxt}>SAVE CHANGES</Text>
                </TouchableOpacity>
              </View>

            </Content>



        
        </Container>
      </SafeAreaView>
    )
  }
}