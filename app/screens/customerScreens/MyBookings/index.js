import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button,Title, Segment, Content, Text, Card, CardItem, View } from 'native-base';
import styles from './style';
import * as colors from "../../../constants/colors"
import {TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
export default class MyBookings extends Component {


    constructor(props) {
        super(props)
        this.state = {
            data: []
        }
    }

    componentWillMount() {
        this.setState({
            data: [
                {
                    arrivalTime: "Nov 16,2019/12:30PM",
                    name: "William Jackson",
                    jobAddress: "xyz street, OH",
                    phoneNumber: "123-1232-12"

                }, {
                    arrivalTime: "Nov 17,2019/12:30PM",
                    name: "Jack",
                    jobAddress: "abc street, AZ",
                    phoneNumber: "123-3333-12"

                }, {
                    arrivalTime: "Nov 18,2019/12:30PM",
                    name: "William Turner",
                    jobAddress: "def street, NY",
                    phoneNumber: "123-1111-12"

                }
            ]
        })


    }

    render(){
        return(
            <Container>
                <Header style={{ backgroundColor: '#fff' }}>
                    <Left>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                    <Icon size={28} style={{ color: '#000', }} name='angle-left' />
                    </TouchableOpacity>

                    </Left>
                    <Body>
                <Title style={styles.HeaderTitle}>MY BOOKINGS</Title>
              </Body>
                    <Right>
                        <Button transparent>
                        <Icon size={24} style={{ color: '#b61925' }} name='bell' />
                        </Button>
                    </Right>
                </Header>

                <Segment style={{ backgroundColor: '#fff', marginTop: '5%'}}>
                    <Button first style={styles.tabButtonLeft}>
                        <Text style={styles.tabText}>Upcoming</Text>
                    </Button>
                   
                    <Button first style={styles.tabButtonRight}>
                        <Text style={styles.tabText}>Completed</Text>
                    </Button>
                </Segment>

                <Content>
                    
                    {
                        this.state.data.map((item, index) =>
                //   <TouchableOpacity onPress={this.props.navigation.navigate('scheduleDetails')}>
                            <CardItem style={styles.containerContent}>
                                <View style={{ flexDirection: "row" }}>
                                    <View style={{ flex: 1, marginRight: 5,}}>
                                        <Text style={{ justifyContent: 'space-evenly',fontWeight:'800'}}>
                                             JOB DATE/TIME:
                                </Text>
                                        <Text style={{fontSize:14, marginTop:'5%',marginLeft:'4%' }}>
                                          Experiment Name:
                                </Text>
                                        <Text style={{ fontSize:14,marginLeft:'4%'}}>
                                           Job Address:
                                </Text>
                                        {/* <Text style={{ justifyContent: 'flex-end',}}>
                                            Phone Number:
                                </Text> */}
                                    </View>
                                    <View style={{ flex: 1, justifyContent: "flex-start", alignItems: "flex-start", marginLeft: 3, }}>
                                        <Text style={{ fontWeight:'800', fontSize:15}}>
                                            {item.arrivalTime}
                                        </Text>
                                        <Text style={{ justifyContent: 'flex-start',fontSize:14, marginTop:'5%'}}>
                                            {item.name}
                                        </Text>
                                        <Text style={{ justifyContent: 'flex-start',fontSize:14, }}>
                                            {item.jobAddress}
                                        </Text>
                                        {/* <Text style={{ justifyContent: 'flex-start',}}>
                                            {item.phoneNumber}
                                        </Text> */}
                                    </View>
                                </View>
                            </CardItem>
                            // </TouchableOpacity>
                       
                        )
                    }
                    
    
                </Content>
            </Container>
        );
    }
}