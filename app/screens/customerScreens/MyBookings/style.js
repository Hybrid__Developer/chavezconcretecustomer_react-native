import { Dimensions, StyleSheet, } from 'react-native';
import * as colors from "../../../constants/colors"
const window = Dimensions.get('window');


const styles = StyleSheet.create({

    tabButtonLeft:{
        borderColor:colors.primaryColor,
        borderTopLeftRadius:20,
        borderBottomLeftRadius:20,
         height:40,
         width:160,
         justifyContent:'center'
        //  shadowColor: 'black',
         // shadowOffset: { width: 5, height: 5 },
         // shadowOpacity: 0.5,
         // shadowRadius: 2,
        //  elevation: 3,
    },
    tabButtonRight:{
        borderColor:colors.primaryColor,
        borderTopRightRadius:20,
        borderBottomRightRadius:20,
        height:40,
        width:160,
        justifyContent:'center'
    },
    tabButoonMiddle:{
        borderColor:colors.primaryColor,
        height:40,
    },
    tabText:{
        color:'#000'
    },
    containerContent:{
        height: 150,
        width: '96%',
        borderWidth: 1,
        marginLeft: '2%',
        borderColor:colors.primaryColor,
        borderRadius:6,
        borderBottomWidth:1,
        marginBottom: 10,
        elevation:10,
        marginTop:'3%'

    },
    HeaderTitle :
    { 
        color:colors.primaryColor ,
     width: 215 }
})
export default styles
