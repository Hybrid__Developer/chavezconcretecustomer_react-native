import { Dimensions, StyleSheet, } from 'react-native';
const window = Dimensions.get('window');
import * as utility from "../../../utility/index"
import * as colors from "../../../constants/colors"
const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
      },
      container: {
        // flex:1,
        // alignItems: 'center',
        // //justifyContent: 'center',
        // alignSelf:'stretch',
        height: utility.deviceHeight,
        width: utility.deviceWidth
      },
      backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
      },
      logoContainer: {
        alignItems: 'center',
        alignSelf: 'stretch',
        //marginTop:30,
        justifyContent: 'center',
        flexGrow: 1,
        marginTop: 35,
      },
      logo: {
        width: 250,
        height: 190,
      },
    SectionStyle: {
       backgroundColor: '#f2dede',
        color: 'white',
  
        elevation: 8,
        borderRadius: 10,
     
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
    },
    ImageStyle: {
        padding: 10,
        margin: 5,
        color: '#000'
    },
    loginTxtInput: {
        backgroundColor: 'transparent',
        color:colors.primaryColor ,
        flex: 1
    },
    InputBar: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    LoginBtn: {
        height: 50,
        backgroundColor:'#fff' ,
        marginTop: '10%',
        borderRadius:40,
        // borderRadius: 8,
       
    
        elevation: 10,
        marginLeft: 40,
        marginRight: 40,

    },
    AJ: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    LoginBtnTxt: {
        color:colors.primaryColor ,
        fontSize: 25,
        textAlign: 'center',
        fontWeight:'bold'
    },
    LoginBtn1: {
        backgroundColor: '#ca0606',
        marginLeft: 70,
        marginRight: 60,
        borderColor: 'white',
        borderRadius: 10,
        borderWidth: 1,
    },
    SectionStyle1: {
        color: 'white',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',

    },
    LoginBtnTxt1: {
        color: '#fff',
        fontSize: 18,
        textAlign: 'center',

    },
})
export default styles
