import React, { Component } from "react";
import { View, Text, SafeAreaView,TextInput,ScrollView,ImageBackground,
    Image,TouchableOpacity,} from "react-native";
import styles from "./style";
import Icon from 'react-native-vector-icons/FontAwesome'
import * as commonApi from "../../../store/commonApi"
import * as api from "../../../constants/api"
import * as utility from "../../../utility/index"


export default class SignUp extends Component {
    constructor (props){
        super(props);
            this.state={
           name:'',
           email:'',
           password:'',
           confirmpassword:'',
           deviceToken:''
            }
   
    }
    register = () => {
    if(utility.isFieldEmpty("")){
         console.log("is field empty")   
       if(utility.isValidEmail("")){
           alert("Please enter valid email address")
                    }
         if(utility.isValidComparedPassword("")){
           alert("Passwords doesnot match!!")
         }         
        const options = {
            headers : {
                "Content-Type": "application/json",
              }
            }
          body ={
            name:this.state.name,
            email:this.state.email,
            password:this.state.password,
            deviceToken:this.state.deviceToken
          }
       
       let response =  commonApi.postDataApi(api.BASE_URL + "/users/register","",body,options )
    console.log("resgister -response", response)
    
}
    else {
        alert("please enter all the fields")
    }

    }
  
     

    
  render() {
    return (
        <SafeAreaView behavior="padding" style={styles.wrapper} >
        <ImageBackground source={require('../../../assets/bg.png')}  style={styles.backgroundImage}>
            <View style={styles.logoContainer}>
                <Image source={require('../../../assets/logo.png')} style={styles.logo} />
            </View>
          <ScrollView>
              <View style={{ flex: 1 }}>
                  
                  <View style={{paddingLeft: '8%', paddingRight: '8%', marginTop: '8%'}}>
                  <View style={{paddingBottom: '3%'}}>
                          <View style={styles.SectionStyle}>
                              <Icon name='user' size={20} style={styles.ImageStyle} />
                              <TextInput require
                                  placeholder='Name'
                                  placeholderTextColor='#000'
                                  onChangeText={(name) => this.setState({ name })}
                                  style={styles.loginTxtInput}
                                  
                              />
                          </View>
                      </View>
                      <View style={{paddingBottom: 15}}>
                         

                          <View style={styles.SectionStyle}>
                              <Icon name='envelope' size={20} style={styles.ImageStyle} />
                              <TextInput require
                                  placeholder=' Email Address'
                                  placeholderTextColor='#000'
                                  onChangeText={(email) => this.setState({ email })}
                                  style={styles.loginTxtInput}
                              />
                          </View>
                      </View>

                      <View style={{ paddingBottom: 15 }}>
                          <View style={styles.SectionStyle}>
                              <Icon name='lock' size={30} style={styles.ImageStyle} />
                              <TextInput require
                                  placeholder=' Password'
                                  placeholderTextColor='#000'
                                  onChangeText={(password) => this.setState({ password })}
                                  secureTextEntry
                                  style={styles.loginTxtInput}
                              />
                          </View>
                      </View>
                      <View style={{ paddingBottom: 1 }}>
                          <View style={styles.SectionStyle}>
                              <Icon name='lock' size={30} style={styles.ImageStyle} />
                              <TextInput require
                                  placeholder='Confirm Password'
                                  placeholderTextColor='#000'
                                  onChangeText={(confirmpassword) => this.setState({ confirmpassword })}
                                  secureTextEntry
                                  style={styles.loginTxtInput}
                              />
                          </View>
                      </View>
                  </View>
                  
                  <TouchableOpacity style={[styles.LoginBtn, styles.AJ]} onPress={() => this.register()} >
                      <Text style={styles.LoginBtnTxt}>SIGNUP</Text>
                  </TouchableOpacity>
              </View>
              <View style={{  paddingLeft: "16%", marginTop: "7%",flexDirection:'row'}}>
              <Text style={{
                                        color: '#000',
                                        fontSize: 17,
                                        
                                        // fontWeight: 'bold'
                                    }}>Already Have an account?
                                    </Text>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('SignIn')}  >
                      <Text style={{
                          color: '#000',
                          fontSize: 17,
                          textDecorationLine: 'underline'
                        //   fontWeight:'bold'
                      }}> SignIn
                  </Text>
                  </TouchableOpacity>
              </View>
            
              </ScrollView>
      </ImageBackground>
 
  </SafeAreaView>

    );
  }
}
