import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button,Title,Text} from 'native-base';
import styles from './style'
import { View, TextInput, TouchableOpacity } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
// import { createDrawerNavigator } from 'react-navigation'  
export default class changePassword extends Component {
    render() {
        return (
            <Container>
                <Header style={{ backgroundColor: '#fff' }}>
                 <Left>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                    <Icon size={28} style={{ color: '#b61925', }} name='angle-left' />
                    </TouchableOpacity>
                    </Left>
                    <Body>
                        <Title style={styles.HeaderTitle}>Concrete Calculator</Title>
                    </Body>
                    <Right>
                        <Button transparent>
                        <Icon size={24} style={{ color: '#b61925' }} name='bell' />
                        </Button>
                    </Right>
                </Header>
                <View style={{ marginTop: '10%' }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.Textstyle}>Wall length:</Text>
                        <TextInput keyboardType='numeric'
                            style={styles.scheduleTextInput}></TextInput>
                        <Text style={styles.feetStyle}>feet</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.Textstyle}>Wall Height:</Text>
                        <TextInput keyboardType='numeric'
                            style={styles.scheduleTextInput}></TextInput>
                        <Text style={styles.feetStyle}>feet</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ fontSize: 20, marginTop: '6%', marginLeft: '2%' }}>Wall Thickness:</Text>
                        <TextInput keyboardType='numeric'
                            style={styles.scheduleTextInput}></TextInput>
                        <Text style={styles.feetStyle}>feet</Text>
                    </View>
                </View>
                <View style={{ marginTop: '25%', marginBottom: '20%' }}>
                    <TouchableOpacity style={[styles.LoginBtn, styles.AJ]} onPress={() => this.props.navigation.navigate('concreteCalci')}>
                        <Text style={styles.LoginBtnTxt}>CALCULATE</Text>
                    </TouchableOpacity>
                </View>
            </Container>
        )
    }
}