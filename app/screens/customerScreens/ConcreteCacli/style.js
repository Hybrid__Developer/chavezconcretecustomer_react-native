import { Dimensions, StyleSheet, } from 'react-native';
import * as colors from "../../../constants/colors"
const window = Dimensions.get('window');
const styles = StyleSheet.create({
    HeaderTitle :{ color:colors.primaryColor , width: 230 },
    scheduleTextInput:{borderWidth:1,borderColor:colors.primaryColor,
         height:30,width:140,marginLeft:'2%', marginTop:'4%',borderRadius:10},
         loginTxtInput: {
            backgroundColor: 'transparent',
            color: '#b61925',
            flex: 1
        },
        InputBar: {
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
        },
        LoginBtn: {
            height: 50,
            backgroundColor: 'white',
            marginTop: 10,
            borderRadius:60,
            // borderRadius: 8,
            borderColor:colors.primaryColor ,
            borderWidth: 1,
            elevation: 10,
            marginLeft: 40,
            marginRight: 40,
    
        },
        forgotBtn: {
            height: 50,
            backgroundColor: 'white',
            marginTop: 20,
            borderRadius:60,
            // borderRadius: 8,
            borderColor: "#fff",
            borderWidth: 1,
            elevation: 10,
            marginLeft: 40,
            marginRight: 40,
    
        },
        AJ: {
            alignItems: 'center',
            justifyContent: 'center',
        },
        LoginBtnTxt: {
            color:colors.primaryColor ,
            fontSize: 25,
            textAlign: 'center',
            fontWeight:'bold'
        },
        Textstyle:{ fontSize:20, marginTop: '6%', marginLeft: '10%' },
        feetStyle:{ fontSize:20, marginTop: '6%', marginLeft: '1%' }
})
export default styles