import { Dimensions, StyleSheet, } from 'react-native';
import * as colors from "../../../constants/colors"
const window = Dimensions.get('window');
const styles = StyleSheet.create({
    scheduleTextInput:
    {   borderWidth:1,
        // borderRadius:8,
        borderColor:colors.primaryColor, 
        height:40, 
        width:'53%',
        marginLeft:'1%', 
       
    },
    PickerView:
    {
     marginLeft: '3%',
    //  marginTop: '3%', 
     borderColor: colors.primaryColor, 
      borderWidth: 1,
       height: 40 ,
       width:'53%',
       justifyContent:'center'
    //    borderRadius:8
    },
    PickerView1:
    {
     marginLeft: '8%',
    //  marginTop: '3%', 
     borderColor: colors.primaryColor, 
      borderWidth: 1,
       height: 35 ,
       width:'40%',
       justifyContent:'center'
    //    borderRadius:8
    },
    datePicker:{ 
        marginLeft:10, 
        borderWidth: 1, 
        // borderRadius:8,
        borderColor: colors.primaryColor, 
        height: 40,
         marginTop: '1%', 
         width: '55%' ,
         justifyContent:'center'
        },
        pickertext:{ fontSize: 18, marginTop: 8, marginLeft: '1%', fontWeight: '800' },



    stepperButton:{

        backgroundColor: '#05cccf',
        height:20,
        // marginLeft: '5%',
       
    },
    stepperView: {
        // flex: 1,
        height:25,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor:'red',
        // justifyContent: 'flex-start',
        marginLeft:'5%',
        // borderRadius:10,
        borderWidth:1,
        borderColor:colors.primaryColor,
   
        
    },
    HeaderTitle :{ color:colors.primaryColor , width: 215 },
    pickerViewStyle:{
        borderColor: colors.primaryColor,
        borderWidth:1,
        borderRadius:10,
        width: 40,
         height: 45,
          justifyContent: 'center',
           marginTop: 16, 
           marginLeft: '5%' 
        },
        information:  {  
            borderWidth:1,
            borderColor:colors.primaryColor, 
            height:40, 
            width:340,
            // marginLeft:'2%', 
            marginTop:'2%',
            // borderRadius:8
        },
        AJ: {
            alignItems: 'center',
            justifyContent: 'center',
        },
        LoginBtnTxt: {
            color:colors.primaryColor,
            fontSize: 25,
            // textAlign: 'center',
            fontWeight:'bold'
        },
        LoginBtn: {
            height:50,
            backgroundColor:"#fff" ,
            // marginTop: '5%',
            borderRadius:40,
            elevation: 10,
            marginLeft: 40,
            marginRight: 40,
            justifyContent:'center',
            alignItems:'center'
    
        },
        EquipmentDetailslView: {
            height: 200,
            width: '96%',
            // backgroundColor:'red',
            borderWidth: 1,
            marginLeft: '2%',
            borderColor: '#b61925',
            borderRadius: 10,
            shadowColor: 'black',
            shadowOffset: { width: 5, height: 5 },
            shadowOpacity: 0.5,
            shadowRadius: 2,
            elevation: 3,
            marginBottom: 10,
            marginTop: '5%'
        },
        EquipmentDetailsHeading: {
            justifyContent: 'flex-start',
            marginLeft: "1%",
            // fontWeight: 'bold'
            fontSize: 18,
            // marginTop: '3%'
        },
stepper: {height:25 , backgroundColor:colors.primaryColor, borderWidth:1,width:30, justifyContent:'center',alignItems:'center'}
       
})
export default styles