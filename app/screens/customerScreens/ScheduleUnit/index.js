import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button, DatePicker, Title, Content, Text, Picker,Card } from 'native-base';
import styles from './style'
import { View, TextInput, TouchableOpacity } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
import * as utility from "../../../utility/index"
import * as colors from "../../../constants/colors"
// import { createDrawerNavigator } from 'react-navigation'  
export default class ScheduleUnit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: '',
            value: '',
            count: 0,
            chosenDate: new Date(),
            select: ''
        };
    }
    onValue(value) {
        this.setState({
            select: value
        });
    }
    onValueChange(value) {
        this.setState({
            selected: value
        });


        this.setDate = this.setDate.bind(this);
    }
    setDate(newDate) {
        this.setState({ chosenDate: newDate });
    }
    increment = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    decrement = () => {
        this.setState({
            count: this.state.count - 1
        })
    }
    render() {
        return (
            <Container style={{height:utility.deviceHeight, width:utility.deviceWidth}} >
                <Header style={{ backgroundColor: '#fff' }}>
                    <Left>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                            <Icon size={28} style={{ color: '#000', }} name='angle-left' />
                        </TouchableOpacity>
                    </Left>
                    <Body>
                        <Title style={styles.HeaderTitle}>SCHEDULE UNIT</Title>
                    </Body>
                    <Right>
                        <Button transparent>
                            <Icon size={24} style={{ color: '#b61925' }} name='bell' />
                        </Button>
                    </Right>
                </Header>







                <Content style={{marginTop:'4%', marginLeft:'2%'}}>



                    <View style={{ flexDirection: 'row',height:'8%'}}>
                       
                            <Text style={styles.pickertext}>From Date/Time:</Text>
                            <View style={styles.datePicker}>
                                <DatePicker
                                    defaultDate={new Date(2018, 4, 4)}
                                    minimumDate={new Date(2018, 1, 1)}
                                    maximumDate={new Date(2018, 12, 31)}
                                    locale={"en"}
                                    timeZoneOffsetInMinutes={undefined}
                                    modalTransparent={false}
                                    animationType={"fade"}
                                    androidMode={"default"}
                                    placeHolderText="Select date"
                                    textStyle={{ color: "black" }}
                                    placeHolderTextStyle={{ color: "#d3d3d3" }}
                                    onDateChange={this.setDate}
                                    disabled={false} />
                            </View>
                       
                        </View>
                     

            

                    <View style={{ flexDirection: 'row', marginTop:'10%',height:'10%'}}>
                        <Text style={{ fontSize: 18, marginTop:'2%'}}>Address of Project:</Text>
                        <TextInput 
                          style={styles.scheduleTextInput}></TextInput>
                    </View>


                    <View style={{ flexDirection: 'row', marginTop:'8%', height:'10%'}}>
                        <Text style={{ fontSize: 18, marginTop:'2%'}} >Select Equipment:</Text>

                        <View style={styles.PickerView}>
                            <Picker
                                mode="dropdown"
                                iosIcon={<Icon name="arrow-down" />}
                                placeholder="31m concrete pump"
                                placeholderStyle={{ color: "#bfc6ea" }}
                                placeholderIconColor="#007aff"
                                // style={{ width: 150 }}
                                selectedValue={this.state.selected}
                                onValueChange={this.onValueChange.bind(this)}>
                                <Picker.Item label="31m concrete pump" value="key0" />
                                <Picker.Item label="31m concrete pump" value="key1" />
                                <Picker.Item label="31m concrete pump" value="key2" />
                                <Picker.Item label="31m concrete pump" value="key3" />
                                <Picker.Item label="31m concrete pump" value="key4" />
                            </Picker>
                        </View>
                    </View>


                   <View style={{marginTop:'9%'}}>
                  
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ marginLeft: '15%', }}>
                            <Text style={{ marginBottom: '3%', fontSize:18 }}>3" X 10' system: </Text>
                        </View>
                            <View style={styles.stepperView}>
                                <TouchableOpacity onPress={this.increment}>
                                <View style={styles.stepper}>
                                    <Text style={{color:'#fff'}}>+</Text></View>                                                        
                                </TouchableOpacity>
                                <Text > {this.state.count} </Text>
                                <TouchableOpacity onPress={this.increment}>
                                <View style={styles.stepper}>
                                    <Text style={{color:'#fff'}}>-</Text></View>                              
                                </TouchableOpacity>                                           
                        </View>
                    </View>
                    </View>
                    <View style={{marginTop:'4%'}}>
                  
                  <View style={{ flexDirection: 'row' }}>
                      <View style={{ marginLeft: '15%', }}>
                          <Text style={{ marginBottom: '3%', fontSize:18 }}>4" X 10' system: </Text>
                      </View>
                          <View style={styles.stepperView}>
                              <TouchableOpacity onPress={this.increment}>
                              <View style={styles.stepper}>
                                  <Text style={{color:'#fff'}}>+</Text></View>                                                        
                              </TouchableOpacity>
                              <Text > {this.state.count} </Text>
                              <TouchableOpacity onPress={this.increment}>
                              <View style={styles.stepper}>
                                  <Text style={{color:'#fff'}}>-</Text></View>                              
                              </TouchableOpacity>                                           
                      </View>
                  </View>
                  </View>
                  <View style={{marginTop:'4%'}}>
                  
                  <View style={{ flexDirection: 'row' }}>
                      <View style={{ marginLeft: '15%', }}>
                          <Text style={{ marginBottom: '3%', fontSize:18 }}>5" X 10' system: </Text>
                      </View>
                          <View style={styles.stepperView}>
                              <TouchableOpacity onPress={this.increment}>
                              <View style={styles.stepper}>
                                  <Text style={{color:'#fff'}}>+</Text></View>                                                        
                              </TouchableOpacity>
                              <Text > {this.state.count} </Text>
                              <TouchableOpacity onPress={this.increment}>
                              <View style={styles.stepper}>
                                  <Text style={{color:'#fff'}}>-</Text></View>                              
                              </TouchableOpacity>                                           
                      </View>
                  </View>
                  </View>
                  <View style={{ flexDirection: 'row', marginTop:'5%', height:'10%', marginLeft:'25%'}}>
                        <Text style={{ fontSize: 18, marginTop:'2%'}} >Wall Pipe:</Text>

                        <View style={styles.PickerView1}>
                            <Picker
                                mode="dropdown"
                                iosIcon={<Icon name="arrow-down" />}
                                placeholder="Yes"
                                placeholderStyle={{ color: "#bfc6ea" }}
                                placeholderIconColor="#007aff"
                                // style={{ width: 150 }}
                                selectedValue={this.state.selected}
                                onValueChange={this.onValueChange.bind(this)}>
                                <Picker.Item label="Yes" value="key0" />
                                <Picker.Item label="No" value="key1" />
                                                       </Picker>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row',  height:'10%', marginLeft:'20%'}}>
                        <Text style={{ fontSize: 18, marginTop:'2%'}} >Mud Snake:</Text>

                        <View style={styles.PickerView1}>
                            <Picker
                                mode="dropdown"
                                iosIcon={<Icon name="arrow-down" />}
                                placeholder="Yes"
                                placeholderStyle={{ color: "#bfc6ea" }}
                                placeholderIconColor="#007aff"
                                // style={{ width: 150 }}
                                selectedValue={this.state.selected}
                                onValueChange={this.onValueChange.bind(this)}>
                                <Picker.Item label="Yes" value="key0" />
                                <Picker.Item label="No" value="key1" />
                                                       </Picker>
                        </View>
                    </View>

                
                    <View style={{ marginTop: 10,marginLeft:'1%',height:80}}>
                        <Text style={{justifyContent: 'flex-start',fontSize:18}}>Additional Pour Information:</Text>
                        <TextInput 
                          style={styles.information}></TextInput>
                    </View>
                    <View style={{height:100, marginTop:'10%',marginBottom:'40%'}}>
                                <TouchableOpacity style={[styles.LoginBtn, styles.AJ]} onPress={() => this.props.navigation.navigate('Home')}>
                                    <Text style={styles.LoginBtnTxt}>SCHEDULE</Text>
                                </TouchableOpacity>
                            </View>
                </Content>
            </Container>
        )
    }
}