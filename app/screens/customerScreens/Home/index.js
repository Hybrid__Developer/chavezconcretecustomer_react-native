import React, { Component } from 'react';
import { Container, Drawer, Header, Left, Body, Right, Button, Icon, Title, Content, Card, CardItem, Text } from 'native-base';
// import { DrawerActions } from 'react-navigation-drawer';
import SideBar from "../SideBar"
import { withNavigation, SafeAreaView } from 'react-navigation';
import { Image } from 'react-native'
import * as utility from "../../../utility/index"
import styles from './style'
import { TouchableOpacity } from 'react-native-gesture-handler';


class Home extends Component {
    componentWillMount() {
        this.setState({
            data: [
                {
                    Image: require('../../../assets/2.png'),
                    name: 'BOOM PUMPS',
                    Types1: '1. 31m schwing',
                    Types2: '2. 39m schwing',
                    Types3: '3. 46m schwing',
                    Types4: '4. 52m schwing',

                },
                {
                    Image: require('../../../assets/3.png'),
                    name: 'TELEBELT CONVEYER',
                    Types1: '1. TB 130 Telebelt',
                    Types2: '',
                    Types3: '',

                },
                {
                    Image: require('../../../assets/4.png'),
                    name: 'CONCRETE CALCULATORS',
                    Types1: '1. Concrete slab calculator',
                    Types2: '2. Concrete footing calculator',
                    Types3: '3. Concrete wall calculator',
                    Types4: '4. Concrete steps calculator',
                    navigationRoute:"concreteCalci"

                },

            ]
        })
    }
    closeDrawer() {
        this._drawer._root.close()
    };
    openDrawer() {
        this._drawer._root.open()
    };
    render() {

        return (

            <Container style={{ height: utility.deviceHeight, width: utility.deviceWidth }}>
                <Drawer ref={(ref) => { this._drawer = ref; }}
                    content={<SideBar navigator={this._navigator}></SideBar>}
                    onClose={() => this.closeDrawer} >
                    <Header style={{ backgroundColor: '#fff' }}>
                        <Left>

                            <Button transparent onPress={() => this.openDrawer()}>
                                <Icon style={{ color: '#000' }} name='menu' />
                            </Button>
                        </Left>
                        <Body>
                            <Image source={require("../../../assets/logo.png")} style={{ height: 40, width: 60, marginLeft: '60%' }}></Image>
                        </Body>
                        <Right>
                            <Button transparent>
                                <Icon style={{ color: '#000' }} name='md-notifications' />
                            </Button>
                        </Right>
                    </Header>
                    <Content>
                        <Card style={{ borderRadius: 10, }} >
                            <Image source={require('../../../assets/1.png')}
                                style={{ width: "100%", height: 135, borderRadius: 10, elevation: 10 }} />
                        </Card>

                        <CardItem style={styles.bookingCard} >

                            <Image source={require("../../../assets/completed_schedule.png")} style={{ height: 25, width: 25 }} />
                           <TouchableOpacity >
                            <Text style={{ fontSize: 20, marginLeft: '2%' }}>
                                VIEW MY BOOKINGS
                                            </Text>
                                            </TouchableOpacity>
                        </CardItem>
                        {
                              this.state.data.map((item, index) =>
                              <TouchableOpacity style={{ marginLeft: '3%' }} onPress={() => this.props.navigation.navigate(item.navigationRoute)}  >
                                <CardItem cardBody style={styles.detailcard}>
                                 
                                    <Image source={item.Image} style={styles.imagecard} />
                                    <Body>
                                        <Text style={styles.mainType}>{item.name}</Text>
                                        <Text style={styles.typeStyle}>{item.Types1}</Text>
                                        <Text style={styles.typeStyle}>{item.Types2}</Text>
                                        <Text style={styles.typeStyle}>{item.Types3}</Text>
                                        <Text style={styles.typeStyle}>{item.Types4}</Text>
                                    </Body>
                                </CardItem>
                                </TouchableOpacity>
                            )}

                    </Content>
                </Drawer>
            </Container>




        );
    }
}
export default withNavigation(Home);