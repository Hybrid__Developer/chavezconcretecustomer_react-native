import { Dimensions, StyleSheet, } from 'react-native';
import * as colors from "../../../constants/colors"
const window = Dimensions.get('window');
const styles = StyleSheet.create({
    HeaderTitle:
    {
        color: colors.primaryColor,
        width: 200
    },


    bookingCard:
    {
        height: 80,
        borderColor: '#b61925',
        borderWidth: 1,
        borderRadius: 7,
        justifyContent: 'center',
        elevation: 10,
        marginLeft: '2%',
        marginRight: '2%',
        marginTop: '4%',
        flexDirection: 'row'
    },
    cardstyle: {
        marginLeft: '3%',
        marginRight: '3%',
        marginTop: '3%',
        borderRadius: 10,
        elevation: 10,
        marginBottom: '10%',
        height: '90%'
    },
    detailcard: {
        height: 120,
        marginTop: '3%',
        borderColor: '#fff',
        borderWidth: 1,
        elevation: 10,
        borderRadius: 10,
        marginLeft: '2%',
        marginRight: '2%',
        marginBottom: 20
    },
    imagecard: {
        height: 88,
        width: 100,
        // marginTop: '4%',
        marginLeft: '4%',
        borderColor: '#b61925',
        borderWidth: 1,

    },
    typeStyle: {
        color: "grey",
        marginLeft: '4%',
        fontSize: 14
    },
    mainType: {
        fontSize: 16,
        marginLeft: '4%',
        marginTop: '4%',
        width: 250,
        color: '#959595',
        fontWeight: '700'
    }
})
export default styles