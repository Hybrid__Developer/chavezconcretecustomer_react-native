import React, { Component } from 'react';
import { Text, ListItem, Right, Left, Icon, Button, Body, Switch } from 'native-base';
import { View, Image, TouchableOpacity } from 'react-native';
import { withNavigation } from 'react-navigation';
import * as utility from "../../utility/index"
import * as colors from "../../constants/colors"
// import { Actions } from 'react-native-router-flux';
// import { TouchableHighlight } from 'react-native-gesture-handler';

class SideBar extends Component {
  componentWillMount() {
    this.setState({
      data: [
        {
          name: "Home",
          icon:require("../../assets/home.png"),
          navigationRoute:'Home'

        }, {
          name: "My Bookings",
          icon: require("../../assets/completed_schedule.png"),
          navigationRoute:'MyBookings'

        }, {
          name: "Schedule Equipments",
          icon: require("../../assets/company_schedule.png"),
          navigationRoute:'ScheduleUnit' 
        },

        {
          name: "Change Password",
          icon:  require("../../assets/change_password.png"),
          navigationRoute:'ChangePassword'

        },
        {
          name: "My Profile",
          icon:  require("../../assets/my_profile.png"),
          navigationRoute:'MyProfile'

        },
        {
          name: "Contact Us",
          icon: require("../../assets/contact_us.png")
         
        },
        {
          name: "Log-out",
          icon: require("../../assets/logout.png")
         
        },

      ]
    })
  }
  render() {

    return (
     
      <View style={{ backgroundColor: '#fff', height: utility.deviceHeight }}>
        <View style={ {
        alignItems: 'center',
        alignSelf: 'stretch',
        //marginTop:30,
        justifyContent: 'center',
        // flexGrow: 1,
        marginTop:20
       }} >
         
                        <Image source={require('../../assets/logo.png')} style={ { width: 250,
                          height: 190,}} />
                    </View>
       
        <View style={{ borderColor: colors.primaryColor, borderBottomWidth: 1, marginTop:20 }}></View>
        {/* <ScrollView> */}
        {
          this.state.data.map((item, index) =>
            <ListItem icon noBorder style={{marginTop:'3%'}}>

              <Left style={{ flex:2,justifyContent: 'flex-start'}} >
                <Button style={{ backgroundColor: "#fff" }}>
                <Image source ={item.icon } style={{ height: 20.7, width:18.4} }/>
                </Button>
                <Body>
                  <TouchableOpacity style={{ marginLeft: '3%' }} onPress={() => this.props.navigation.navigate(item.navigationRoute)}  >
                    <Text> {item.name}</Text>
                  </TouchableOpacity>
                </Body>
              </Left>

            </ListItem>
          )
        }
   {/* </ScrollView> */}
      </View>
   
    )
  }
}

export default withNavigation(SideBar);

