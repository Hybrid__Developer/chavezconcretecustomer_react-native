import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button, Title, Content, Input, Item, Text, Card, CardItem } from 'native-base';
import styles from './style'
import Icon from 'react-native-vector-icons/FontAwesome';
import { View, ImageBackground, TouchableOpacity, Image } from "react-native";
import * as utility from "../../../utility/index"
// import { createDrawerNavigator } from 'react-navigation'  
export default class MyProfile extends Component {
    render() {
        return (
            <Container style={{ height: utility.deviceHeight,
                width: utility.deviceWidth}} >
                <Header style={{ backgroundColor: '#fff' }}>
                    <Left>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                    <Icon size={28} style={{ color: '#000', }} name='angle-left' />
                    </TouchableOpacity>

                    </Left>
                    <Body>
                        <Title style={{ color: '#b61925', width: 220, fontSize: 18 }}>MY PROFILE</Title>
                    </Body>
                    <Right>
                        <Button transparent>
                        <Icon size={24} style={{ color: '#b61925' }} name='bell' />
                        </Button>
                    </Right>
                </Header>

                <View style={styles.cardStyle}>
                    <View>
                        <Image source={require('../../../assets/profile_image.png')} style={{ width: '100%', height: 150 }} />
                    </View>

                    <View style={styles.drawerHeader}>
                        <View style={styles.drawerImageView}>
                            <TouchableOpacity>
                                <ImageBackground source={require('../../../assets/backgroundImage.png')} style={styles.drawerImageBackground}>

                                </ImageBackground>
                            </TouchableOpacity>
                        </View>
                    </View>


                </View>
                <View style={{ paddingLeft: 245, marginTop: '5%' }}>
                    <TouchableOpacity><Text style={{ color: '#b61925' }}>Change Image</Text></TouchableOpacity>
                </View>
                <Content style={{ marginTop: '5%' }}>
                    <Item regular style={styles.itemStyle}>
                        <Input
                           
                            placeholder='Name' />
                    </Item>
                    <Item regular style={styles.itemStyle}>
                        <Input
                           
                            placeholder='Address' />
                    </Item>
                    <Item regular style={styles.itemStyle}>
                        <Input
                          
                            placeholder='Email' />
                    </Item>
                    <Item regular style={styles.itemStyle}>
                        <Input
                           
                            placeholder='Phone' />
                    </Item>

                    <View style={{ marginTop: '5%', marginBottom: '20%' }}>
                        <TouchableOpacity style={[styles.LoginBtn, styles.AJ]} onPress={() => this.props.navigation.navigate('Home')}>
                            <Text style={styles.LoginBtnTxt}>SAVE CHANGES</Text>
                        </TouchableOpacity>
                    </View>

                </Content>




            </Container>
        )
    }
}