import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button, Title, Content, Input, Item, Text, Card, CardItem } from 'native-base';
import styles from './style'
import Icon from 'react-native-vector-icons/FontAwesome';
import { View, ImageBackground, TouchableOpacity, Image } from "react-native";

// import { createDrawerNavigator } from 'react-navigation'  
export default class MyProfile extends Component {
    render() {
        return (
            <Container>
                <Header style={{ backgroundColor: '#fff' }}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.navigate('Home')}>
                            <Icon size={28} style={{ color: '#000', }} name='angle-left' />
                        </Button>

                    </Left>
                    <Body>
                        <Title style={{ color: '#b61925', width: 230, fontSize: 24 }}>MY PROFILE</Title>
                    </Body>
                    <Right>
                        <Button transparent>
                            <Image source={require('../../../assets/bell.png')} style={{ width: 20, height: 23, marginTop: '15%' }} />
                        </Button>
                    </Right>
                </Header>

                <View style={styles.cardStyle}>
                    <View>
                        <Image source={require('../../../assets/profile_image.png')} style={{ width: '100%', height: 180, borderRadius: 10 }} />
                    </View>

                    <View style={styles.drawerHeader}>
                        <View style={styles.drawerImageView}>
                            <TouchableOpacity>
                                <ImageBackground source={require('../../../assets/backgroundImage.png')} style={styles.drawerImageBackground}>

                                </ImageBackground>
                            </TouchableOpacity>
                        </View>
                    </View>


                </View>
                <View style={{ paddingLeft: 250, marginTop: '10%', }}>
                    <TouchableOpacity><Text style={{ color: '#b61925' }}>Change Image</Text></TouchableOpacity>
                </View>
                <Content style={{ marginTop: '5%' }}>
                    <Item regular style={styles.itemStyle}>
                        <Input

                            placeholder='Name' />
                    </Item>
                    <Item regular style={styles.itemStyle}>
                        <Input

                            placeholder='Email' />
                    </Item>
                    <Item regular style={styles.itemStyle}>
                        <Input

                            placeholder='Phone' />
                    </Item>
                    <Item regular style={styles.itemStyle}>
                        <Input

                            placeholder='ACPA Card:' />
                    </Item>

                    <View style={{ marginTop: '2%', marginBottom: '10%' }}>
                        <TouchableOpacity style={[styles.LoginBtn, styles.AJ]} onPress={() => this.props.navigation.navigate('ChangePassword')}>
                            <Text style={styles.LoginBtnTxt}>SAVE CHANGES</Text>
                        </TouchableOpacity>
                    </View>

                </Content>




            </Container>
        )
    }
}
