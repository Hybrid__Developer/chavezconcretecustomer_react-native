import { Dimensions, StyleSheet, } from 'react-native';
const window = Dimensions.get('window');
const styles = StyleSheet.create({
    LoginBtn: {
        height: 50,
        backgroundColor: '#b61925',
        marginTop: 100,
        borderRadius: 60,
        // borderRadius: 8,
        borderColor: "#b61925",
        borderWidth: 1,
        elevation: 10,
        marginLeft: 40,
        marginRight: 40,

    },
    forgotBtn: {
        height: 50,
        backgroundColor: 'white',
        marginTop: 20,
        borderRadius: 60,
        // borderRadius: 8,
        borderColor: "#fff",
        borderWidth: 1,
        elevation: 10,
        marginLeft: 40,
        marginRight: 40,

    },
    AJ: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    LoginBtnTxt: {
        color: 'white',
        fontSize: 25,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    itemView: {
        marginTop: '4%', marginLeft: '2%', marginRight: '2%', borderColor: '#b61925',
        borderWidth: 1, borderRadius: 10, elevation: 3

    }
})
export default styles