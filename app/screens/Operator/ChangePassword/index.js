import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button, Title, Content, Input, Item, Text } from 'native-base';
import styles from './style'
import { View, TouchableOpacity, Image } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
// import { createDrawerNavigator } from 'react-navigation'  
export default class changePassword extends Component {
  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#fff' }}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.navigate('Home')}>
              <Icon size={28} style={{ color: '#000', }} name='angle-left' />
            </Button>

          </Left>
          <Body>
            <Title style={{ color: '#b61925', width: 230, fontSize: 24 }}>Change Password</Title>
          </Body>
          <Right>
            <Button transparent >
              <Image source={require('../../../assets/bell.png')} style={{ width: 20, height: 23, }} />
            </Button>
          </Right>
        </Header>

        <Content style={{ marginTop: '5%' }}>
          <Item regular style={styles.itemView}>
            <Input
              secureTextEntry
              placeholder='Old Password' />
          </Item>
          <Item regular style={styles.itemView}>
            <Input
              secureTextEntry
              placeholder='New Password' />
          </Item>
          <Item regular style={styles.itemView}>
            <Input
              secureTextEntry
              placeholder='Confirm Password' />
          </Item>
          <View style={{ marginTop: '5%', marginBottom: '20%' }}>
            <TouchableOpacity style={[styles.LoginBtn, styles.AJ]} onPress={() => this.props.navigation.navigate('ChangePassword')}>
              <Text style={styles.LoginBtnTxt}>SAVE CHANGES</Text>
            </TouchableOpacity>
          </View>

        </Content>

      </Container>
    )
  }
}