import React, { Component } from 'react';
import { Text, ListItem, Right, Left, Icon, Button, Body, Switch } from 'native-base';
import { View, Image, TouchableOpacity } from 'react-native';
import { withNavigation } from 'react-navigation';
import * as utility from "../../utility/index"
// import { Actions } from 'react-native-router-flux';
// import { TouchableHighlight } from 'react-native-gesture-handler';

class SideBar extends Component {
  componentWillMount() {
    this.setState({
      data: [
        {
          name: "Upcoming Schedule",
          icon: require("../../assets/company_schedule.png"),
          navigateRoute: 'OperatorHome'
        }, {
          name: "Completed Schedule",
          icon: require("../../assets/completed_schedule.png"),
          navigateRoute: 'CompletedSchedule'
        },

        {
          name: "Change Password",
          icon: require("../../assets/change_password.png"),
          navigateRoute: 'OperatorChangePassword'
        },
        {
          name: "My Profile",
          icon: require("../../assets/my_profile.png"),
          navigateRoute: 'OperatorMyProfile'
        },
        {
          name: "Contact Us",
          icon: require("../../assets/contact_us.png"),

        },
        {
          name: "Log-out",
          icon: require("../../assets/logout.png"),

        },

      ]
    })
  }
  render() {
    console.log('utility.deviceHeight', utility.deviceHeight)
    return (
      <View style={{ backgroundColor: '#fff', height: utility.deviceHeight }}>
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <Image source={require('../../assets/logo.png')} style={{ width: "70%", height: 175, marginTop: '15%' }} />
        </View>
        <View style={{ borderColor: 'grey', borderBottomWidth: 1, marginTop: '12%' }}></View>
        {
          this.state.data.map((item, index) =>
            <ListItem icon noBorder style={{ marginTop: '3%' }}>

              <Left style={{ flex: 1, justifyContent: 'flex-start' }} >
                <View style={{ width: 35, height: 35, alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={item.icon} style={{ height: 28, width: 28, }} />
                </View>
                < Body >
                  <TouchableOpacity style={{ marginLeft: '3%' }} onPress={() => this.props.navigation.navigate(item.navigateRoute)} >
                    <Text> {item.name}</Text>
                  </TouchableOpacity>
                </Body>
              </Left>

            </ListItem>
          )
        }

      </View>
    )
  }
}
export default withNavigation(SideBar);

