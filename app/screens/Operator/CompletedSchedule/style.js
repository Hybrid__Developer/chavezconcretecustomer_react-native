import { Dimensions, StyleSheet, } from 'react-native';
const window = Dimensions.get('window');


const styles = StyleSheet.create({
    HeaderTitle: {
        color: '#b61925',
        fontSize: 18,
        fontWeight: 'bold',
        justifyContent: 'center',
        alignSelf: 'center',
        marginLeft: '10%'
    },
    tabButtonLeft: {
        borderColor: '#b61925',
        borderTopLeftRadius: 20,
        borderBottomLeftRadius: 20,
        height: 40,
        // marginLeft: '3%'
        //  shadowColor: 'black',
        // shadowOffset: { width: 5, height: 5 },
        // shadowOpacity: 0.5,
        // shadowRadius: 2,
        //  elevation: 3,
    },
    tabButtonRight: {
        borderColor: '#b61925',
        borderTopRightRadius: 20,
        borderBottomRightRadius: 20,
        height: 40,
        // marginRight: '3%'
    },
    tabButoonMiddle: {
        borderColor: '#b61925',
        height: 40,
    },
    tabText: {
        color: '#000'
    },
    containerContent: {
        height: 130,
        width: '96%',
        // backgroundColor:'red',
        borderWidth: 1,
        marginLeft: '2%',
        borderColor: '#b61925',
        borderRadius: 8,
        elevation: 4,
        marginBottom: 15,

    },
})
export default styles

