import React, { Component } from 'react';
import { Container, Header, Drawer, Left, Body, Right, Button, Icon, Title, Segment, Content, Text, Card, CardItem, View } from 'native-base';
import styles from './style';
import SideBar from '../SideBar';
import { Image } from 'react-native';
export default class CompletedScheduleDetails extends Component {
    closeDrawer() {
        this._drawer._root.close()
    };
    openDrawer() {
        this._drawer._root.open()
    };

    render() {
        return (
            <Container>
                <Drawer ref={(ref) => { this._drawer = ref; }}
                    content={<SideBar navigator={this._navigator}></SideBar>}
                    onClose={() => this.closeDrawer} >
                    <Header style={{ backgroundColor: '#fff' }}>
                        <Left>
                            <Button transparent onPress={() => this.openDrawer()}>
                                <Icon style={{ color: '#000' }} name='menu' />
                            </Button>

                        </Left>
                        <Title style={styles.HeaderTitle}>SCHEDULE DETAILS</Title>
                        <Right>
                            <Button transparent >
                                <Image source={require('../../../assets/bell.png')} style={{ width: 20, height: 23, }} />
                            </Button>
                        </Right>
                    </Header>


                    <View style={styles.CustomerDetailView}>
                        <View style={{ flexDirection: "row" }}>
                            <View style={styles.CustomerDetailLeftView}>

                                <Text style={styles.CustomerDetailLeftText}>
                                    Customer Name:
                                </Text>
                                <Text style={styles.CustomerDetailLeftText}>
                                    Contact Number:
                                </Text>
                                <Text style={styles.CustomerDetailLeftText}>
                                    Arrival Date:
                                </Text>
                                <Text style={styles.CustomerDetailLeftText}>
                                    Start Time:
                                </Text>
                                <Text style={styles.CustomerDetailLeftText}>
                                    {/* {item.jobAddress} */}
                                    Job Address:
                                        </Text>
                            </View>
                            <View style={styles.CustomerDetailRightView}>

                                <Text style={styles.CustomerDetailRightText}>
                                    {/* {item.name} */}zxc
                                        </Text>
                                <Text style={styles.CustomerDetailRightText}>
                                    {/* {item.jobAddress} */}123456
                                        </Text>
                                <Text style={styles.CustomerDetailRightText}>
                                    {/* {item.phoneNumber} */}5/12/2019
                                        </Text>
                                <Text style={styles.CustomerDetailRightText}>
                                    {/* {item.phoneNumber} */}10:00 A.M
                                        </Text>
                                <Text style={styles.CustomerDetailRightText}>
                                    {/* {item.phoneNumber} */}xyz
                                        </Text>
                            </View>

                        </View>
                    </View>

                    <View style={styles.EquipmentDetailslView}>
                        <Text style={styles.EquipmentDetailsHeading}>Equipment Details:</Text>
                        <View style={{ justifyContent: 'center', alignSelf: 'center' }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#b61925' }}>31 m Concrete Pump</Text>
                        </View>
                        <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
                            <Text>3" X 10' : 12</Text>
                            <Text>4" X 10' : 12</Text>
                            <Text>5" X 10' : 12</Text>
                            <Text>Wallpipe : NO</Text>
                            <Text>Mudsnake : Yes</Text>
                        </View>
                    </View>
                    <View style={styles.JobConditionView}>
                        <Text style={styles.JobConditionHeading}>Job Condition & Notes: </Text>
                        <Text style={styles.JobConditionText}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the </Text>
                    </View>
                </Drawer>
            </Container>
        );
    }
}
