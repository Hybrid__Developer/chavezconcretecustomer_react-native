import { Dimensions, StyleSheet, } from 'react-native';
const window = Dimensions.get('window');


const styles = StyleSheet.create({
    forgotBtn: {
        height: 50,
        backgroundColor: 'white',
        marginTop: 20,
        borderRadius:60,
        // borderRadius: 8,
        borderColor: "#fff",
        borderWidth: 1,
        elevation: 10,
        marginLeft: 40,
        marginRight: 40,

    },
    AJ: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    LoginBtnTxt: {
        color: '#05cccf',
        fontSize: 25,
        textAlign: 'center',
        fontWeight:'bold'
    },
    HeaderTitle:{
        color: '#05cccf', 
        fontSize: 18,
        fontWeight:'bold', 
        justifyContent:'center', 
        alignSelf:'center',
        marginLeft: '18%'
    },
    tabButtonLeft:{
        borderColor:'#05cccf',
        borderTopLeftRadius:20,
        borderBottomLeftRadius:20,
         height:40,
    },
    tabButtonRight:{
        borderColor:'#05cccf',
        borderTopRightRadius:20,
        borderBottomRightRadius:20,
        height:40,
    },
    tabButoonMiddle:{
        borderColor:'#05cccf',
        height:40,
    },
    tabText:{
        color:'#000'
    },
    containerContent:{
        height: 150,
        width: '96%',
        // backgroundColor:'red',
        borderWidth: 1,
        marginLeft: '2%',
        borderColor:'#05cccf',
        borderRadius: 10,
        shadowColor: 'black',
        shadowOffset: { width: 5, height: 5 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 2,
        marginBottom: 10,
       
        
    },
})
export default styles
