import { Dimensions, StyleSheet, } from 'react-native';
const window = Dimensions.get('window');


const styles = StyleSheet.create({

    HeaderTitle: {
        color: '#b61925',
        fontSize: 22,
        fontWeight: 'bold',
        justifyContent: 'center',
        alignSelf: 'center',
        marginLeft: '18%'
    },

    scheduleTextInput: {
        borderWidth: 1,
        borderColor: '#b61925',
        height: 100,
        width: 140,
        marginLeft: '13%',
        marginTop: '4%'
    },
    PickerView: {
        marginLeft: '14%',
        marginTop: '3%',
        borderColor: '#b61925',
        borderWidth: 1,
        height: 40
    },
    datePicker: {
        marginLeft: 60,
        borderWidth: 1,
        borderColor: '#b61925',
        height: 40,
        marginTop: '4%',
        width: 145
    },
    CustomerDetailView: {
        height: 150,
        width: '96%',
        marginLeft: '2%',
        borderColor: '#b61925',
        borderRadius: 10,
        borderWidth: 1,
        elevation: 3,
        marginTop: '5%',
    },
    CustomerDetailLeftView: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "flex-start",
        marginRight: 5,
        marginTop: '2%',
        marginLeft: '5%',
    },
    CustomerDetailRightView: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "flex-start",
        marginLeft: 5,
        marginTop: '2%'
    },
    CustomerDetailLeftText: {
        justifyContent: 'flex-end',
        marginTop: '2%'
    },
    CustomerDetailRightText: {
        justifyContent: 'flex-start',
        marginTop: '2%'
    },
    EquipmentDetailslView: {
        height: 180,
        width: '96%',
        // backgroundColor:'red',
        borderWidth: 1,
        marginLeft: '2%',
        borderColor: '#b61925',
        borderRadius: 10,
        elevation: 3,
        marginTop: '5%'


    },
    EquipmentDetailsHeading: {
        justifyContent: 'flex-start',
        marginLeft: "5%",
        // fontWeight: 'bold'
        fontSize: 18
    },
    JobConditionView: {
        height: 100,
        width: '96%',
        // backgroundColor:'red',
        borderWidth: 1,
        marginLeft: '2%',
        borderColor: '#b61925',
        borderRadius: 10,
        elevation: 3,
        marginTop: '5%'
    },
    JobConditionHeading: {
        marginLeft: '5%',
        fontSize: 18
    },
    JobConditionText: {
        // padding: '4%',
        marginLeft: "5%",
        letterSpacing: 0.8
    },
    text: {
        fontSize: 30,
        alignSelf: 'center',
        color: 'red'
    },
    forgotBtn: {
        height: 40,
        backgroundColor: 'white',
        // marginTop: 10,
        borderRadius: 50,
        // borderRadius: 8,
        borderColor: "#b61925",
        borderWidth: 1,
        elevation: 5,
        marginLeft: 70,
        marginRight: 70,


    },
    AJ: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    LoginBtnTxt: {
        color: '#b61925',
        fontSize: 25,
        textAlign: 'center',
        fontWeight: 'bold'
    },
})
export default styles