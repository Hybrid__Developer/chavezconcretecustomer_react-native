import React, { Component } from 'react';
import { Container, Drawer, Header, Left, Body, Right, Button, Icon, Title, Segment, Content, Text, Card, CardItem, View } from 'native-base';
import styles from './style';
import { TouchableOpacity, Image } from 'react-native'
import { DrawerActions } from 'react-navigation-drawer';
import SideBar from '../SideBar'
export default class Home extends Component {

    closeDrawer() {
        this._drawer._root.close()
    };
    openDrawer() {
        this._drawer._root.open()
    };
    render() {
        return (
            <Container>
                <Drawer ref={(ref) => { this._drawer = ref; }}
                    content={<SideBar navigator={this._navigator}></SideBar>}
                    onClose={() => this.closeDrawer} >
                    <Header style={{ backgroundColor: '#fff' }}>
                        <Left>

                            <Button transparent onPress={() => this.openDrawer()}>
                                <Icon style={{ color: '#000' }} name='menu' />
                            </Button>
                        </Left>
                        {/* <Title style={styles.HeaderTitle}>Company Schedule</Title> */}
                        {/* <Body></Body> */}
                        <View style={{ justifyContent: 'center', alignItems: 'center', marginLeft: '30%', }}>
                            <Image source={require('../../../assets/small_logo.png')} style={{ height: 52, width: 67, alignSelf: 'center', }} />
                        </View>
                        <Right>

                            <Button transparent >
                                <Image source={require('../../../assets/bell.png')} style={{ width: 20, height: 23, }} />
                            </Button>
                        </Right>
                    </Header>

                    {/* <Segment> */}
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ textAlign: 'center', color: '#b61925', fontSize: 24, fontWeight: 'bold', marginTop: '5%' }}>COMPANY SCHEDULE</Text>
                    </View>

                    <View style={{ marginTop: '5%' }}>
                        <Button full style={styles.homeButton1} onPress={() => this.props.navigation.navigate('ScheduledJobs')}>
                            <Text style={styles.homeButtonText}> View Scheduled JObs</Text>
                        </Button>
                        <Button full style={styles.homeButton2} onPress={() => this.props.navigation.navigate('PendingJobs')}>
                            <Text style={styles.homeButtonText}>Schedule A New Job</Text>
                        </Button>
                    </View>
                    {/* </Segment> */}
                </Drawer>
            </Container>
        )
    }
}