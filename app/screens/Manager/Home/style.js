import { Dimensions, StyleSheet, } from 'react-native';
const window = Dimensions.get('window');


const styles = StyleSheet.create({

    HeaderTitle: {
        color: '#b61925',
        fontSize: 18,
        fontWeight: 'bold',
        justifyContent: 'center',
        alignSelf: 'center',
        marginLeft: '18%'
    },
    homeButton1: {
        // marginTop: 400,
        backgroundColor: '#b61925',
        margin: 10,
        shadowColor: 'black',
        height: "22%",
        // shadowOffset: { width: 5, height: 5 },
        // shadowOpacity: 0.5,
        // shadowRadius: 2,
        elevation: 5,
        borderRadius: 5,
    },
    homeButton2: {
        backgroundColor: '#b61925',
        margin: 10,
        shadowColor: 'black',
        height: "22%",
        borderRadius: 5,
        // shadowOffset: { width: 5, height: 5 },
        // shadowOpacity: 0.5,
        // shadowRadius: 2,
        elevation: 5,
    },
    homeButtonText: {
        fontSize: 20,
        fontWeight: 'bold'
    }

})
export default styles