import { Dimensions, StyleSheet, } from 'react-native';
const window = Dimensions.get('window');


const styles = StyleSheet.create({

    HeaderTitle: {
        color: '#b61925',
        fontSize: 24,
        fontWeight: 'bold',
        justifyContent: 'center',
        alignSelf: 'center',
        marginLeft: '18%'
    },
    CustomerDetailView: {
        height: 80,
        width: '96%',
        // backgroundColor:'red',
        borderWidth: 1,
        marginLeft: '3%',
        borderColor: '#b61925',
        borderRadius: 10,
        shadowColor: 'black',
        elevation: 3,
        marginBottom: 10,
        marginTop: '5%',
        flexDirection: 'row'
    },
})


export default styles