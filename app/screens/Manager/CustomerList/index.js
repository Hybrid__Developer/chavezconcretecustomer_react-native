import React, { Component } from 'react';
import { Container, Header, Left, Input, Item, Button, Body, Right, Title, Segment, Content, Picker, Text, Card, CardItem, View, DatePicker } from 'native-base';
import styles from './style';
import { TextInput, TouchableOpacity, Image } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
export default class CustomerList extends Component {
    render() {
        return (
            <Container>
                <Header style={{ backgroundColor: '#fff' }}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.navigate('Home')}>
                            <Icon size={28} style={{ color: '#000', }} name='angle-left' />
                        </Button>

                    </Left>
                    <Title style={styles.HeaderTitle}>Customer List</Title>
                    <Right>
                        <Button transparent>
                            <Image source={require('../../../assets/bell.png')} style={{ width: 20, height: 23, marginTop: '15%' }} />

                        </Button>
                    </Right>
                </Header>
                <View style={{ marginLeft: '5%', marginRight: '5%', marginTop: '5%', borderColor: '#b61925' }}>
                    <Item searchBar rounded style={{ borderColor: '#b61925' }}>
                        <Icon right size={20} style={{ color: '#b61925', marginLeft: '5%', }} name="search" />
                        <Input placeholder="Search by name" />
                        {/* <Icon name="ios-people" /> */}
                    </Item>
                </View>
                <View style={styles.CustomerDetailView}>
                    <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: '2%', marginLeft: '3%' }}>
                        <Image rounded source={require('../../../assets/user.png')} style={{ height: 50, width: 50, }} />
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'flex-start', marginLeft: '5%' }}>
                        <Text style={{ fontWeight: 'bold' }}>William jackson</Text>
                        <Text >Phone: 1234-5554</Text>
                        <Text> Address: 1515 upland Sylvania</Text>
                    </View>
                </View>

                <View style={styles.CustomerDetailView}>
                    <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: '2%', marginLeft: '3%' }}>
                        <Image rounded source={require('../../../assets/user.png')} style={{ height: 50, width: 50, }} />
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'flex-start', marginLeft: '5%' }}>
                        <Text style={{ fontWeight: 'bold' }}>William jackson</Text>
                        <Text >Phone: 1234-5554</Text>
                        <Text> Address: 1515 upland Sylvania</Text>
                    </View>
                </View>

                <View style={styles.CustomerDetailView}>
                    <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: '2%', marginLeft: '3%' }}>
                        <Image rounded source={require('../../../assets/user.png')} style={{ height: 50, width: 50, }} />
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'flex-start', marginLeft: '5%' }}>
                        <Text style={{ fontWeight: 'bold' }}>William jackson</Text>
                        <Text >Phone: 1234-5554</Text>
                        <Text> Address: 1515 upland Sylvania</Text>
                    </View>
                </View>


                {/* <Button transparent>
                    <Text>Search</Text>
                </Button> */}
            </Container>
        );
    }
}