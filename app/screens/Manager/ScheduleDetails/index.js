import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button, Title, Segment, Content, Text, Card, CardItem, View } from 'native-base';
// import styles from './style';
import { Image } from "react-native";
import styles from './style';
import Icon from 'react-native-vector-icons/FontAwesome';
export default class ScheduleDetails extends Component {
    render() {
        return (
            <Container>
                <Header style={{ backgroundColor: '#fff' }}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.navigate("ScheduledJobs")}>
                            <Icon size={28} style={{ color: '#000', }} name='angle-left' />
                        </Button>

                    </Left>
                    <Title style={styles.HeaderTitle}>SCHEDULE DETAILS</Title>
                    <Right>
                        <Button transparent onPress={() => this.props.navigation.navigate("ScheduleDetails")}>
                            <Image source={require('../../../assets/bell.png')} style={{ width: 20, height: 23, marginTop: '15%' }} />

                        </Button>
                    </Right>
                </Header>
                <View style={{ flexDirection: 'row', marginTop: '5%' }}>
                    <View style={{ marginLeft: '5%' }} >
                        <Text style={{ fontSize: 20, fontWeight: 'bold', color: '#b61925' }}>Job Status: </Text>
                    </View>
                    <View style={{ marginTop: '1%' }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Pending </Text>
                    </View>
                </View>

                <Card style={styles.CustomerDetailView}>
                    <View style={{ flexDirection: "row" }}>
                        <View style={styles.CustomerDetailLeftView}>

                            <Text style={styles.CustomerDetailLeftText}>
                                Customer Name:
                                </Text>
                            <Text style={styles.CustomerDetailLeftText}>
                                Contact Number:
                                </Text>
                            <Text style={styles.CustomerDetailLeftText}>
                                Arrival Date:
                                </Text>
                            <Text style={styles.CustomerDetailLeftText}>
                                Start Time:
                                </Text>
                            <Text style={styles.CustomerDetailLeftText}>
                                {/* {item.jobAddress} */}
                                Job Address:
                                        </Text>
                        </View>
                        <View style={styles.CustomerDetailRightView}>

                            <Text style={styles.CustomerDetailRightText}>
                                {/* {item.name} */}zxc
                                        </Text>
                            <Text style={styles.CustomerDetailRightText}>
                                {/* {item.jobAddress} */}123456
                                        </Text>
                            <Text style={styles.CustomerDetailRightText}>
                                {/* {item.phoneNumber} */}5/12/2019
                                        </Text>
                            <Text style={styles.CustomerDetailRightText}>
                                {/* {item.phoneNumber} */}10:00 A.M
                                        </Text>
                            <Text style={styles.CustomerDetailRightText}>
                                {/* {item.phoneNumber} */}xyz
                                        </Text>
                        </View>

                    </View>
                </Card>

                <Card style={styles.EquipmentDetailslView}>
                    <Text style={styles.EquipmentDetailsHeading}>Equipment Details:</Text>
                    <Text style={{ color: '#b61925', fontWeight: 'bold', textAlign: 'center', fontSize: 18, marginBottom: '2%' }}>31m Concrete Pump</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ marginLeft: '30%', }}>
                            <Text style={{ marginBottom: '3%' }}>3" X 10': </Text>
                            <Text style={{ marginBottom: '3%' }}>3" X 10': </Text>
                            <Text style={{ marginBottom: '3%' }}>3" X 10': </Text>
                            <Text style={{ marginBottom: '3%' }}>Wallpipe:</Text>
                            <Text style={{ marginBottom: '3%' }}>Mudsnake:</Text>
                        </View>
                        <View style={{ marginLeft: "5%", }}>
                            <Text style={{ marginBottom: '3%' }}>12</Text>
                            <Text style={{ marginBottom: '3%' }}>21</Text>
                            <Text style={{ marginBottom: '3%' }}>14</Text>
                            <Text style={{ marginBottom: '3%' }}>No</Text>
                            <Text style={{ marginBottom: '3%' }}>Yes</Text>
                        </View>
                    </View>
                </Card>
                <Card style={styles.JobConditionView}>
                    <Text style={styles.JobConditionHeading}>Job Condition & Notes: </Text>
                    <Text style={styles.JobConditionText}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the </Text>
                </Card>
            </Container>
        );
    }
}
