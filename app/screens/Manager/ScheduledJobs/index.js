import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button, Title, Segment, Content, Text, Card, CardItem, View } from 'native-base';
import styles from './style';
import { TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
export default class ScheduledJobs extends Component {


    constructor(props) {
        super(props)
        this.state = {
            data: []
        }
    }

    componentWillMount() {
        this.setState({
            data: [
                {
                    arrivalTime: "Nov 16, 2019/12:30 PM",
                    name: "William Jackson",
                    phoneNumber: "123-1232-12",
                    jobAddress: "xyz street, OH Mall Road Shimla ",

                }, {
                    arrivalTime: "Nov 17, 2019/12:30 PM",
                    name: "Jack",
                    phoneNumber: "123-3333-12",
                    jobAddress: "abc street, AZ  Mall Road Shimla ",

                }, {
                    arrivalTime: "Nov 18, 2019/12:30 PM",
                    name: "William Turner",
                    phoneNumber: "123-1111-12",
                    jobAddress: "def street, NY  Mall Road Shimla ",

                }
            ]
        })


    }

    render() {
        return (
            <Container>
                <Header style={{ backgroundColor: '#fff' }}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.navigate("Home")}>
                            <Icon size={28} style={{ color: '#000', }} name='angle-left' />
                        </Button>

                    </Left>
                    {/* <Body style={{ backgroundColor: 'red', justifyContent:'center', marginLeft: 20 }}> */}
                    <Title style={{ color: '#b61925', fontSize: 22, fontWeight: 'bold', justifyContent: 'center', alignSelf: 'center', marginLeft: '18%' }}>SCHEDULED JOBS</Title>
                    {/* </Body> */}
                    <Right>
                        <Button transparent>
                            <Image source={require('../../../assets/bell.png')} style={{ width: 20, height: 23, marginTop: '15%' }} />

                        </Button>
                    </Right>
                </Header>

                <Segment style={{ backgroundColor: '#fff', marginTop: '10%', marginLeft: '5%', marginRight: '5%' }}>
                    <Button first style={styles.tabButtonLeft}>
                        <Text style={styles.tabText}>TODAY</Text>
                    </Button>
                    <Button style={styles.tabButoonMiddle}>
                        <Text style={styles.tabText}>THIS WEEK</Text>
                    </Button>
                    <Button first style={styles.tabButoonMiddle}>
                        <Text style={styles.tabText}>THIS MONTH</Text>
                    </Button>
                    <Button first style={styles.tabButtonRight}>
                        <Text style={styles.tabText}>ALL</Text>
                    </Button>
                </Segment>

                <Content>

                    {
                        this.state.data.map((item, index) =>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate("ScheduleDetails")} >
                                <Card style={styles.containerContent}>
                                    <View style={{ flexDirection: "row", marginTop: 10 }}>
                                        <View style={{ flex: 1, alignItems: "flex-end", marginRight: 5, }}>
                                            <Text style={{ justifyContent: 'space-evenly', fontWeight: 'bold' }}>
                                                ARRIVAL DATE/TIME:
                                </Text>
                                            <Text style={{ justifyContent: 'flex-end', }}>
                                                Customer Name:
                                </Text>
                                            <Text style={{ justifyContent: 'flex-end', }}>
                                                Phone Number:
                                </Text>
                                            <Text style={{ justifyContent: 'flex-end', }}>
                                                Job Address:
                                </Text>

                                        </View>
                                        <View style={{ flex: 1, justifyContent: "flex-start", alignItems: "flex-start", marginLeft: 5, }}>
                                            <Text style={{ fontSize: 14, fontWeight: 'bold' }}>
                                                {item.arrivalTime}
                                            </Text>
                                            <Text >
                                                {item.name}
                                            </Text>

                                            <Text style={{ justifyContent: 'flex-start', }}>
                                                {item.phoneNumber}
                                            </Text>
                                            <Text style={{ justifyContent: 'flex-start', }}>
                                                {item.jobAddress}
                                            </Text>
                                        </View>
                                    </View>
                                </Card>
                            </TouchableOpacity>
                        )
                    }


                </Content>
            </Container>
        );
    }
}