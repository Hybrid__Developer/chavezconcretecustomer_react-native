import React, { Component } from 'react';
import { Container, Header, Left, Button, Right, Title, Picker, Text, View, DatePicker } from 'native-base';
import styles from './style';
import { TextInput, ScrollView, Image } from "react-native";
// import { ScrollView } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';
// const Item = Picker.Item;
export default class AddNewJob extends Component {

    constructor(props) {
        super(props);
        this.state = {
            count: 0,
            select: ''
            // selected: "key0"
        }
    }

    increment = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    decrement = () => {
        this.setState({
            count: this.state.count - 1
        })
    }
    // onValue(value) {
    //     this.setState({
    //         select: value
    //     });
    // }

    render() {
        return (
            <Container>
                <ScrollView>
                    <Header style={{ backgroundColor: '#fff' }}>
                        <Left>
                            <Button transparent onPress={() => this.props.navigation.navigate('Home')}>
                                <Icon size={28} style={{ color: '#000', }} name='angle-left' />
                            </Button>

                        </Left>
                        <Title style={styles.HeaderTitle}>Add New Job</Title>
                        <Right>
                            <Button transparent>
                                <Image source={require('../../../assets/bell.png')} style={{ width: 20, height: 20, marginTop: '15%' }} />
                                {/* <Icon size={24} style={{ color: '#b61925' }} name='bell' /> */}
                            </Button>
                        </Right>
                    </Header>

                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.textLabel} >
                            Customer Name :
                        </Text>
                        <TextInput style={styles.scheduleTextInput}>

                        </TextInput>
                    </View>


                    <View >
                        <Text style={{ marginTop: '6%', marginLeft: '5%', fontSize: 16 }}> Date/Time</Text>
                        <View style={styles.datePicker}>
                            <DatePicker
                                defaultDate={new Date(2018, 4, 4)}
                                minimumDate={new Date(2018, 1, 1)}
                                maximumDate={new Date(2018, 12, 31)}
                                locale={"en"}
                                timeZoneOffsetInMinutes={undefined}
                                modalTransparent={false}
                                animationType={"fade"}
                                androidMode={"default"}
                                placeHolderText="Select date"
                                textStyle={{ color: "black" }}
                                placeHolderTextStyle={{ color: "#d3d3d3" }}
                                onDateChange={this.setDate}
                                disabled={false} />
                        </View>
                    </View>

                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.textLabel} >
                            Address of Project:
                        </Text>
                        <TextInput style={styles.AddressTextbox}>
                        </TextInput>
                    </View>

                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.textLabel} >
                            Select Equipment :
                        </Text>
                        <TextInput style={styles.AddressTextbox}>
                        </TextInput>

                    </View>


                    <View style={{ marginTop: '15%', flexDirection: 'row', }}>
                        <View style={{ marginLeft: '20%', justifyContent: 'flex-start', }}>
                            <Text>3" X 10' :</Text>
                        </View>
                        <View style={styles.stepperView}>

                            <Button onPress={this.decrement} style={styles.stepperButton} ><Text style={{ alignItems: 'center', justifyContent: "center" }}>-</Text></Button>
                            <Text> {this.state.count} </Text>
                            <Button onPress={this.increment} style={styles.stepperButton}><Text>+</Text></Button>
                        </View>

                    </View>

                    <View style={{ marginTop: '5%', flexDirection: 'row', }}>
                        <View style={{ marginLeft: '20%', justifyContent: 'flex-start', }}>
                            <Text>4" X 10' :</Text>
                        </View>
                        <View style={styles.stepperView}>

                            <Button onPress={this.decrement} style={styles.stepperButton} ><Text>-</Text></Button>
                            <Text> {this.state.count} </Text>
                            <Button onPress={this.increment} style={styles.stepperButton}><Text>+</Text></Button>
                        </View>

                    </View>

                    <View style={{ marginTop: '5%', flexDirection: 'row', }}>
                        <View style={{ marginLeft: '20%', justifyContent: 'flex-start', }}>
                            <Text>5" X 10' :</Text>
                        </View>
                        <View style={styles.stepperView}>

                            <Button onPress={this.decrement} style={styles.stepperButton} ><Text style={{ textAlign: 'center', justifyContent: 'center' }}>-</Text></Button>
                            <Text> {this.state.count} </Text>
                            <Button onPress={this.increment} style={styles.stepperButton}><Text>+</Text></Button>
                        </View>

                    </View>


                    <View >
                        <Text style={styles.textLabel} >
                            Additional Pour Information :
                        </Text>
                        <TextInput multiline={true} numberOfLines={4} style={styles.InfoTextBox}>
                        </TextInput>
                    </View>
                    <View style={{ marginTop: '15%', }}>
                        <Button style={[styles.forgotBtn, styles.AJ]}>
                            <Text style={styles.LoginBtnTxt}>Schedule</Text>
                        </Button>
                    </View>
                </ScrollView>
            </Container>

        );
    }
}