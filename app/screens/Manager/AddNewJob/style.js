import { Dimensions, StyleSheet, } from 'react-native';
const window = Dimensions.get('window');


const styles = StyleSheet.create({

    HeaderTitle: {
        color: '#b61925',
        fontSize: 22,
        fontWeight: 'bold',
        justifyContent: 'center',
        alignSelf: 'center',
        marginLeft: '18%'
    },
    textBoxView: {
        // flexDirection:'row'
    },
    scheduleTextInput: {
        borderWidth: 1,
        borderColor: '#b61925',
        height: 25,
        width: 180,
        marginLeft: '5%',
        marginTop: '4%',
        borderRadius: 5
    },
    AddressTextbox: {
        borderWidth: 1,
        borderColor: '#b61925',
        height: 25,
        width: 180,
        marginLeft: '3%',
        marginTop: '4%',
        borderRadius: 5
    },
    stepperView: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor:'red',
        justifyContent: 'flex-start',
        marginLeft: '5%'

    },
    stepperButton: {
        backgroundColor: '#b61925',
        height: 20,
        // width: 25,
        // marginLeft: '5%',
        // marginTop: '2%',
        justifyContent: 'center',
        // alignSelf: 'center'
        alignContent: 'center',
        alignItems: 'center'
    },

    InfoTextBox: {
        borderWidth: 1,
        borderColor: '#b61925',
        height: 30,
        width: 320,
        marginLeft: '5%',
        marginTop: '4%',
        borderRadius: 5
    },
    textLabel: {
        marginTop: '4%',
        marginLeft: '5%',
        fontSize: 16,
    },
    forgotBtn: {
        height: 40,
        backgroundColor: 'white',
        // marginTop: '10%',
        borderRadius: 50,
        // borderRadius: 8,
        borderColor: "#b61925",
        borderWidth: 1,
        elevation: 5,
        marginLeft: 70,
        marginRight: 70,
        marginBottom: '5%',

    },
    AJ: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    LoginBtnTxt: {
        color: '#b61925',
        fontSize: 25,
        // textAlign: 'center',
        fontWeight: 'bold',
        justifyContent: 'center',
        // alignItems: 'center',
        alignSelf: "center",
        marginBottom: '5%'
        // backgroundColor: "pink",
        // marginTop: '15%'
    },
    datePicker: {
        marginLeft: '5%',
        borderWidth: 1,
        borderColor: '#b61925',
        height: 30,
        marginTop: '4%',
        width: 145,
        // alignContent:'center',
        justifyContent: 'center',
        // alignItems:'center'
        borderRadius: 5
    },
})


export default styles