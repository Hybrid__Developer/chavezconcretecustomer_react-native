import { Dimensions, StyleSheet, } from 'react-native';
const window = Dimensions.get('window');
const styles = StyleSheet.create({

    drawerHeader: {
        justifyContent: 'center',
        alignItems: 'center',

    },
    drawerImageBackground: {
        height: 150,
        width: 150,
        borderRadius: 50,
        // marginTop: 100

    },
    drawerImageView: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 1

    },
    LoginBtn: {
        height: 50,
        backgroundColor: '#b61925',
        marginTop: 10,
        borderRadius: 60,
        // borderRadius: 8,
        borderColor: "#b61925",
        borderWidth: 1,
        elevation: 10,
        marginLeft: 40,
        marginRight: 40,

    },
    forgotBtn: {
        height: 50,
        backgroundColor: 'white',
        marginTop: 20,
        borderRadius: 60,
        // borderRadius: 8,
        borderColor: "#fff",
        borderWidth: 1,
        elevation: 10,
        marginLeft: 40,
        marginRight: 40,

    },
    AJ: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    LoginBtnTxt: {
        color: 'white',
        fontSize: 25,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    itemStyle: {
        marginTop: '4%',
        marginLeft: '2%',
        marginRight: '2%',
        borderColor: '#b61925',
        borderWidth: 1,
        borderWidth: 1,
        borderRadius: 10,
        elevation: 3

    },
    cardStyle: { marginLeft: '3%', marginRight: '3%', marginTop: '3%', backgroundColor: 'pink' }


})
export default styles