import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button, Title, Segment, Content, Text, Card, CardItem, View } from 'native-base';
import styles from './style';
import Icon from 'react-native-vector-icons/FontAwesome';
// import { TouchableOpacity } from 'react-native-gesture-handler';
import { TouchableOpacity, Image } from 'react-native';

export default class PendingJobs extends Component {


    constructor(props) {
        super(props)
        this.state = {
            data: []
        }
    }

    componentWillMount() {
        this.setState({
            data: [
                {
                    arrivalTime: "Nov 16, 2019/12:30 PM",
                    name: "William Jackson",
                    jobAddress: "xyz street, OH",
                    phoneNumber: "123-1232-12"

                }, {
                    arrivalTime: "Nov 17, 2019/12:30 PM",
                    name: "Jack",
                    jobAddress: "abc street, AZ",
                    phoneNumber: "123-3333-12"

                }, {
                    arrivalTime: "Nov 18, 2019/12:30 PM",
                    name: "William Turner",
                    jobAddress: "def street, NY",
                    phoneNumber: "123-1111-12"

                }
            ]
        })


    }

    render() {
        return (
            <Container>
                <Header style={{ backgroundColor: '#fff' }}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.navigate('Home')}>
                            <Icon size={28} style={{ color: 'black', }} name='angle-left' />
                        </Button>

                    </Left>
                    {/* <Body style={{ backgroundColor: 'red', justifyContent:'center', marginLeft: 20 }}> */}
                    <Title style={{ color: '#b61925', fontSize: 22, fontWeight: 'bold', justifyContent: 'center', alignSelf: 'center', marginLeft: '18%' }}>PENDING JOBS</Title>
                    {/* </Body> */}
                    <Right>
                        <Button transparent>
                            <Image source={require('../../../assets/bell.png')} style={{ width: 20, height: 23, marginTop: '15%' }} />

                        </Button>
                    </Right>
                </Header>

                <Segment style={{ backgroundColor: '#fff', marginTop: '5%', marginLeft: '2%', marginRight: '2%' }}>
                    <Button first style={styles.tabButtonLeft}>
                        <Text style={styles.tabText}>THIS WEEK</Text>
                    </Button>
                    {/* <Button style={styles.tabButtonLeft}>
                        <Text style={styles.tabText}>THIS WEEK</Text>
                    </Button> */}
                    <Button first style={styles.tabButoonMiddle}>
                        <Text style={styles.tabText}>THIS MONTH</Text>
                    </Button>
                    <Button first style={styles.tabButtonRight}>
                        <Text style={styles.tabText}>ALL</Text>
                    </Button>
                </Segment>

                <Content>

                    {
                        this.state.data.map((item, index) =>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate("AssignOperator")} >
                                <Card style={styles.containerContent}>
                                    <View style={{ flexDirection: "row", marginTop: 10 }}>
                                        <View style={{ flex: 1, justifyContent: "flex-end", alignItems: "flex-end", marginRight: 5, }}>
                                            <Text style={{ justifyContent: 'space-evenly', }}>
                                                ARRIVAL DATE/TIME:
                                </Text>
                                            <Text style={{ justifyContent: 'flex-end', }}>
                                                Customer Name:
                                </Text>
                                            <Text style={{ justifyContent: 'flex-end', }}>
                                                Job Address:
                                </Text>
                                            <Text style={{ justifyContent: 'flex-end', }}>
                                                Phone Number:
                                </Text>
                                        </View>
                                        <View style={{ flex: 1, justifyContent: "flex-start", alignItems: "flex-start", marginLeft: 5, }}>
                                            <Text style={{ fontSize: 14 }}>
                                                {item.arrivalTime}
                                            </Text>
                                            <Text style={{ justifyContent: 'flex-start', }}>
                                                {item.name}
                                            </Text>
                                            <Text style={{ justifyContent: 'flex-start', }}>
                                                {item.jobAddress}
                                            </Text>
                                            <Text style={{ justifyContent: 'flex-start', }}>
                                                {item.phoneNumber}
                                            </Text>
                                        </View>
                                    </View>
                                </Card>
                            </TouchableOpacity>
                        )
                    }


                </Content>
            </Container>
        );
    }
}