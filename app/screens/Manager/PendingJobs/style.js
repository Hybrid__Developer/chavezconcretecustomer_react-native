import { Dimensions, StyleSheet, } from 'react-native';
const window = Dimensions.get('window');


const styles = StyleSheet.create({

    tabButtonLeft: {
        borderColor: '#b61925',
        borderTopLeftRadius: 20,
        borderBottomLeftRadius: 20,
        height: 40,
        //  width: 150
        //  shadowColor: 'black',
        // shadowOffset: { width: 5, height: 5 },
        // shadowOpacity: 0.5,
        // shadowRadius: 2,
        //  elevation: 3,
    },
    tabButtonRight: {
        borderColor: '#b61925',
        borderTopRightRadius: 20,
        borderBottomRightRadius: 20,
        height: 40,
    },
    tabButoonMiddle: {
        borderColor: '#b61925',
        height: 40,
    },
    tabText: {
        color: '#000'
    },
    containerContent: {
        height: 110,
        width: '96%',
        borderWidth: 1,
        marginLeft: '2%',
        borderColor: '#b61925',
        borderRadius: 10,
        shadowColor: 'black',
        marginBottom: 10

    },
})
export default styles

